require.config({
    baseUrl : '/public/javascript/lib',
    paths : {
        // the left side is the module ID,
        // the right side is the path to
        // the jQuery file, relative to baseUrl.
        // Also, the path should NOT include
        // the '.js' file extension. This example
        // is using jQuery 1.9.0 located at
        // js/lib/jquery-1.9.0.js, relative to
        // the HTML page.
        'jquery' : 'jquery-1.10.2.min',
        'bootstrap' : 'bootstrap.min',
        'lightbox' : 'jquery.lightbox.min'
    },
    shim : {
        'bootstrap' : {
            'deps' : [ 'jquery' ]
        },
        'lightbox' : {
            'deps' : [ 'jquery' ]
        }
    }
});

requirejs([ 'jquery', 'bootstrap' ], function($, _bootstrap) {
    $('.carousel').carousel({
        interval : 7000,
        pause : false,
        wrap : true
    });

    $lightbox = $('.lightbox');
    if ($lightbox.length > 0) {
        requirejs([ 'lightbox' ], function(){
            $lightbox.lightbox();
        });
    }
    
    $('.tooltip-trigger').tooltip();

    function $alert (text, type) {
        if (typeof(type) === "undefined") {
            type = "success";
        }

        $element = $(document.createElement('div'))
                    .addClass('modal')
                    .append(
                        $(document.createElement('div'))
                        .addClass('modal-dialog')
                        .append(
                            $(document.createElement('div'))
                            .addClass('modal-content alert alert-' + type)
                            .append(
                                $(document.createElement('div'))
                                .addClass('modal-header')
                                .append(
                                    $(document.createElement('button'))
                                    .addClass('close')
                                    .attr('data-dismiss', 'modal')
                                    .attr('aria-hidden', true)
                                    .attr('type', 'button')
                                    .text('×'))
                                .append(
                                    $(document.createElement('h4'))
                                    .addClass('modal-title')
                                    .text('Succes !'))
                            )
                            .append(
                                $(document.createElement('div'))
                                .addClass('modal-body alert alert-' + type)
                                .append(
                                    $(document.createElement('p'))
                                    .text(text))
                            )
                        )
                    )
                    .appendTo("#scripts");

        $element.modal();
    }

    $registercollapser = $('input.register-collapser');
    $registercollapser.each(function(i, e){
        var e = $(e);

        if (typeof(e.data('checked')) !== "undefined") {
            e.prop('checked', true);
        }
    });
    $registercollapser.on('click', function(event){
        var t = $(this);
        $registercollapser.prop('checked', false);

        t.prop('checked', true);

        $('.register-collapsable').addClass('gray');
        $('#' + t.val()).removeClass('gray');
    });

    // header basket update function
    function headerBasketUpdate () {
        $headerBasket = $('#header-shopcart-products-list');
        $.ajax({
            "url": "/ajax/cart/list",
            "dataType": "json"
        }).done(function(data){
            if (data.success == true) {
                $('li.shopcart-item', $headerBasket).remove();
                
                for (var i in data.cart) {
                    item = data.cart[i];
                    
                    $headerBasket.prepend(
                        $(document.createElement('li'))
                        .text('- ' + item.code)
                        .addClass('shopcart-item')
                    );
                }

                if (data.count > 0) {
                    $('#shopcart-total-price').text(data.total);
                    $('#shopcart-total-products').text(data.count);
                    $('#shopcart-summary').removeClass('hidden');
                    $('.list-shopcart').removeClass('hidden');
                    $('.no-items').addClass('hidden');
                } else {
                    $('#shopcart-summary').addClass('hidden');
                    $('.list-shopcart').addClass('hidden');
                    $('.no-items').removeClass('hidden');
                }

                $('#header-shopcart-status').html(data.text);
            }
        });
    }

    // generic add to cart function
    $('a.add-to-cart').on('click', function(event) {
        var t = $(this),
            quantity = parseInt($(t.data('quantity')).val()),
            id = parseInt(t.data('id'));
        
        if (quantity > 0) {
            $.ajax({
                "url": "/ajax/cart/add",
                "dataType": "json",
                "data": {
                    "quantity": quantity,
                    "id": id
                }
            }).done(function(data){
                if (data.success == true) {
                    $alert(data.message);
                    
                    headerBasketUpdate();
                }
            });
        }
        
        event.preventDefault();
    });
    
    // generic shopcart basket delete function
    $('a.delete-from-cart').on('click', function(event) {
        var t = $(this),
            id = parseInt(t.data('id'));
        
        $.ajax({
            "url": "/ajax/cart/remove",
            "dataType": "json",
            "data": {
                "id": id
            }
        }).done(function(data){
            if (data.success == true) {
                $alert(data.message);
                
                headerBasketUpdate();
            }
            
            if (typeof(t.data('target')) !== "undefined") {
                $(t.data('target')).remove();
            }
        });
        
        event.preventDefault();
    });

    $('input.update-cart').on('change', function(event) {
        var t = $(this),
            quantity = parseInt(t.val()),
            id = parseInt(t.data('id')),
            target = t.data('target'),
            price = parseFloat(t.data('price'));
        
        if (quantity > 0) {
            $.ajax({
                "url": "/ajax/cart/update",
                "dataType": "json",
                "data": {
                    "quantity": quantity,
                    "id": id
                }
            }).done(function(data){
                if (data.success == true) {
                    $alert(data.message);

                    if (typeof(target) !== "undefined") {
                        var qtyTarget = $(target);
                        
                        if (qtyTarget.length > 0) {
                            qtyTarget.text(price * quantity);
                        }
                    }

                    headerBasketUpdate();
                }
            });
        }
        
        event.preventDefault();
    });

    // order person select
    $('input.person-select').on('click', function(event) {
        var t = $(this);

        $(t.data("class")).addClass("hidden");
        $(t.data("target")).removeClass("hidden");
    });

    // update header products
    headerBasketUpdate();
});