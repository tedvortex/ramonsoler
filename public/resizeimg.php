<?php
$file = dirname(__FILE__) . '/' . $_GET['folder'] . '/' . $_GET['image'] . '.' . $_GET['extension'];
$width = $_GET['width'];
$height = $_GET['height'];
$folder = dirname(__FILE__) . '/' . $_GET['folder'] . '/' . $width;
$imageName = $_GET['image'];
$extension = $_GET['extension'];
$writable = (is_dir($folder) && is_writable($folder . '/'));
$imageinfo = array();

if (is_file($file)) {
    list ($imageWidth, $imageHeight, $ftype) = getimagesize($file, $imageinfo);

    if ($imageinfo) {
        switch ($ftype) {
            case IMAGETYPE_GIF:
                $image = @imagecreatefromgif($file);
                $ext = 'gif';
                break;

            case IMAGETYPE_JPEG:
                $image = @imagecreatefromjpeg($file);
                $ext = 'jpg';
                break;

            case IMAGETYPE_PNG:
                $image = @imagecreatefrompng($file);
                $ext = 'png';
                break;
        }

        $imageWidthRatio = $imageWidth / $width;
        $imageHeightRatio = $imageHeight / $height;

        $dest_x = $dest_y = 0;

        if ($imageWidthRatio > $imageHeightRatio) {
            $imageDesiredWidth = round($imageHeight * $width / $height);
            $imageDesiredHeight = $imageHeight;

            $src_x = ($imageWidth - $imageDesiredWidth) / 2;
            $src_y = 0;
        } else {
            $imageDesiredWidth = $imageWidth;
            $imageDesiredHeight = round($imageWidth *  $height / $width);

            $src_x = 0;
            $src_y = ($imageHeight - $imageDesiredHeight) / 2;
        }

        $imagePlaceholder = imagecreatetruecolor($width, $height);

        if ($imagePlaceholder) {
            imagealphablending($imagePlaceholder, false);
            imagesavealpha($imagePlaceholder, true);
            $transparent = imagecolorallocatealpha($imagePlaceholder, 255, 255, 255, 127);
            imagefilledrectangle($imagePlaceholder, 0, 0, $width, $height, $transparent);
            imagecopyresampled($imagePlaceholder, $image, $dest_x, $dest_y, $src_x, $src_y, $width, $height, $imageDesiredWidth, $imageDesiredHeight);

            $expires = 2592000; //60*60*24*30

            header('Pragma: public');
            header('Cache-Control: maxage=' . $expires);
            header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');
            header('Content-type: image/' . $extension);

            $newFile = ($writable ? $folder . '/' . $imageName . '.' . $extension : null);

            switch ($ftype) {
                case IMAGETYPE_GIF:
                    imagegif($imagePlaceholder, $newFile);
                    break;

                case IMAGETYPE_JPEG:
                    imagejpeg($imagePlaceholder, $newFile, 100);
                    break;

                case IMAGETYPE_PNG:
                    imagepng($imagePlaceholder, $newFile, 9);
                    break;
            }

            imagedestroy($imagePlaceholder);

            if ($writable) {
                readfile($newFile);
            }
        }
    }
}