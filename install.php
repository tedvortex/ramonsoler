<?php
$paths = array(
    '/vendor/symfony/framework-bundle/Symfony/Bundle/FrameworkBundle/Resources/views/Form'
);

foreach ($paths as $path) {
    $shortcut = root . '/src/override' . $path;
    $target = root . $path;

    echo("rm -rf {$target} && ln -s {$shortcut} {$target}");
}

exit();