<?php
define('microtime', microtime(true));
define('root', dirname(__FILE__));

// composer autoload
require_once __DIR__ . '/vendor/autoload.php';

if (is_file(root . '/install.php') && isset($_GET['install'])) {
    require_once root . '/install.php';
}

// create app
$app = new Silex\Application();

// hijack/inject custom application logic here before framework processing is done
// ...
// global init
include_once root . '/src/init.php';

// extend/inject custom application logic here after framework processing is done
// ...
// bootstrap
if ($config['environment'] === 'development') {
    $app->run();
} else {
    $app['http_cache']->run();
}