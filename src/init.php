<?php
// create config
$config = require root . '/src/config.php';

if ($config['environment'] === 'development') {
    $app['debug'] = true;

    error_reporting(E_ALL);
    ini_set("display_errors", true);
}

$autoload = glob(root . '/src/init/*.php');

if (is_array($autoload)) {
    foreach ($autoload as $file) {
        include_once $file;
    }
}

// globals
include_once root . '/src/global.php';

// routing
include_once root . '/src/route.php';

// security inject
include_once root . '/src/security.php';