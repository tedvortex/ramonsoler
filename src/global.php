<?php
$config['public']['link'] = $_SERVER['REQUEST_URI'];

if (is_array($config['public']) && ! empty($config['public'])) {
    foreach ($config['public'] as $key => $value) {
        $app['engine']['slots']->set($key, $value);
    }
}