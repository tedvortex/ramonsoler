<?php
$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'account' => array(
            'pattern' => '^' . $routes['account']['route'],
            'form' => array(
                'login_path' => $routes['login']['route'],
                'check_path' => $routes['account']['route'] . '/login_check',
                'default_target_path' => $routes['account']['route']
            ),
            'logout' => array(
                'logout_path' => $routes['account']['route'] . '/logout'
            ),
            'users' => $app->share(function () use ($app) {
                return \Application\Model\Customer::getInstance($app['zend.adapter']);
            })
        ),
    ),
    'security.access_rules' => array(
        array('^/.+$', 'ROLE_USER'),
        array('^' . $routes['login']['route'] . '$', ''),
    )
));

$app->before(function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $session = $request->getSession();
    $user = null;

    $security = $session->get('_security_account');
    if (null !== $security) {
        $token = unserialize($security);

        if (null !== $token) {
            $user = $token->getUser();
        }
    }

    $app['engine']['slots']->set('_user', $user);
});