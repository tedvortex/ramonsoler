<?php
return array(
    'environment' => 'development',
    'static.path' => 'http://localhost:8000/public',
    'session.storage.save_path' => root . '/tmp/session',
    'session.storage.options' => array(
        'name' => 'ramonsoler',
        'cookie_lifetime' => '604800',
        'cookie_domain' => '.localhost',
        'cookie_path' => '/',
        'cookie_httponly' => true,
    ),
    'webhost' => 'ramonsoler.ro',
    'dbs.options' => array (
        'driver'    => 'pdo_mysql',
        'host'      => 'localhost',
        'dbname'    => 'ramonsoler',
        'user'      => 'root',
        'password'  => 'Q!w2E#r4',
        'charset'   => 'utf8',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "UTF8"'
        ),
    ),
    'public' => array(
        'base.path' => 'http://ramonsoler.ro',
        'ga._setAccount' => 'UA-48543531-1',
        'ga._setDomainName' => 'ramonsoler.ro',
        'header.call' => '0751.080.014 | 0751.048.744'
    ),
);