<?php
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

$PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
$CategoryModel = \Application\Model\Category::getInstance($app['zend.adapter']);
$ShopcartModel = new \Application\Model\Shopcart($cart);
$app['engine']['slots']->set('productCount', count($ShopcartModel->storage));

$constructors = $PageModel->listAll(array('p.constructor <> ""'));
$allCategories = $CategoryModel->listAll(array('id_parent' => null));

$routes = $footerPages = $boxPages = $categories = array();

if (is_array($constructors) && !empty($constructors)) {
    foreach ($constructors as $constructor) {
        $routeExtended = array();
        $routeExtended['action'] = ($constructor['action'] ? $constructor['action'] : 'defaultAction');
        $routeExtended['name'] = $constructor['name'];
        $routeExtended['route_name'] = $constructor['route_name'];
        $routeExtended['route'] = '/' . $PageModel->link($constructor);
        $routeExtended['controller'] = 'Application\\Controller\\' . $constructor['constructor'];
        $route = $app->match($routeExtended['route'], $routeExtended['controller'] . '::' . $routeExtended['action'])
            ->method('GET|POST');

        if ($constructor['is_footer'] == 1) {
            $footerPages[] = $routeExtended;
        }

        if ($constructor['is_box'] == 1) {
            $boxPages[] = $routeExtended;
        }

        if ($constructor['route_name']) {
            $route->bind($constructor['route_name']);
            $routes[$constructor['route_name']] = $routeExtended;
        } else {
            $routes[] = $routeExtended;
        }
    }
}

if ($allCategories !== false) {
    foreach ($allCategories as $c) {
        $c['link'] = /*$routes['categories']['route'] . '/' . */
            $CategoryModel->link($c);
        $c['subcategories'] = $CategoryModel->listAll(array('id_parent' => $c['id_category']));
        $c['has_children'] = false;

        if ($c['subcategories'] !== false) {
            foreach ($c['subcategories'] as $k => $sc) {
                $sc['link'] = /*$routes['categories']['route'] . '/' . */
                    $CategoryModel->link($sc);

                $c['subcategories'][$k] = $sc;

                $app->get('/' . $sc['link'], 'Application\\Controller\\Category::listBySubCategory')
                    ->value('categoryNameSeo', $c['name_seo'])
                    ->value('subCategoryNameSeo', $sc['name_seo']);
            }

            $c['has_children'] = true;

            if ($c['id_parent'] != 0) {
                $app->get('/' . $c['link'], 'Application\\Controller\\Category::listByCategory')
                    ->value('categoryNameSeo', $c['name_seo']);
            } else {
                $app->get('/' . $c['link'], 'Application\\Controller\\Category::listBySubCategory')
                    ->value('categoryNameSeo', $c['name_seo'])
                    ->value('subCategoryNameSeo', $c['name_seo']);
            }
        }

        $categories[] = $c;
    }
}

$app['engine']['slots']->set('categories', $categories);
$app['engine']['slots']->set('routes', $routes);
$app['engine']['slots']->set('footerPages', $footerPages);
$app['engine']['slots']->set('boxPages', $boxPages);

// disable basic routing
//$app->get($routes['categories']['route'] . '/{categoryNameSeo}', 'Application\\Controller\\Category::listByCategory');
//$app->get($routes['categories']['route'] . '/{categoryNameSeo}/{subCategoryNameSeo}', 'Application\\Controller\\Category::listBySubCategory');
//$app->get($routes['categories']['route'] . '/{categoryNameSeo}/{subCategoryNameSeo}/{productNameSeo}', 'Application\\Controller\\Category::listByCategoryProduct');

// enable product lookup method
$app->before(function (Request $request) use ($app) {
    $ProductModel = \Application\Model\Product::getInstance($app['zend.adapter']);

    $return = $ProductModel->listRowByNameSeo($ProductModel->url($request->getRequestUri(), ' ', false));

    if ($return !== false) {
        $return['link'] = $ProductModel->link($return);

        if ($return['categoryParent'] !== null) {
            $app->get($return['link'], 'Application\\Controller\\Category::listByCategoryProduct')
                ->value('categoryNameSeo', $return['rootCategoryNameSeo'])
                ->value('subCategoryNameSeo', $return['categoryNameSeo'])
                ->value('productNameSeo', $return['name_seo']);
        } else {
            $app->get($return['link'], 'Application\\Controller\\Category::listByCategoryProductSimple')
                ->value('categoryNameSeo', $return['categoryNameSeo'])
                ->value('productNameSeo', $return['name_seo']);
        }

        $subRequest = Request::create($return['link']);
        $response = $app->handle($subRequest, HttpKernelInterface::SUB_REQUEST, false);

        return $response;
    }
}, Silex\Application::EARLY_EVENT);

// sitemap route
$app->get('/sitemap.xml', 'Application\\Controller\\Sitemap::defaultAction');

// image resizer routes
$app->get('/resize/{dimension}', 'Application\\Controller\\ProductImage::defaultAction');

// ajax routes
$app->get('/ajax/{module}/{action}', 'Application\\Controller\\Ajax::defaultAction');