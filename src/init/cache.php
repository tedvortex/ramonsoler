<?php
$app->register(new Silex\Provider\HttpCacheServiceProvider(), array(
    'http_cache.cache_dir' => root . '/tmp/cache/',
    'http_cache.esi'       => null,
));