<?php
Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace(
    'JMS\Serializer\Annotation',
    root . '/vendor/jms/serializer/src');

$app['serializer'] = $app->share(function ($app) {
    return JMS\Serializer\SerializerBuilder::create()
        ->setCacheDir(root . '/tmp/serializer')
        ->setDebug(true)
        ->build();
});