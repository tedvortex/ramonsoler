<?php
// use custom 404 page only when not in development environment

if ($config['environment'] !== 'development') {
    $app->error(function(\Exception $e, $code) use ($app) {
        switch ($code) {
            case 400:
                $message = 'Cerere malformata';
                break;

            case 404:
                $message = 'Pagina pe care o cautati nu exista !<br/>Intoarceti-va la meniul principal.';
                break;

            default:
                $message = 'Eroare neasteptata';
        }

        $return = array(
            'name' => 'EROARE ' . $code . ' !',
            'description' => $message
        );

        return $app['engine']->render('404/default.php', $return);
    });
}