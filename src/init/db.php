<?php
$app['zend.adapter'] = new Zend\Db\Adapter\Adapter(array(
    'driver'   => $config['dbs.options']['driver'],
    'database' => $config['dbs.options']['dbname'],
    'username' => $config['dbs.options']['user'],
    'password' => $config['dbs.options']['password'],
    'charset'  => $config['dbs.options']['charset'],
    'driver_options' => $config['dbs.options']['driver_options'],
));