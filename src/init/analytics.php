<?php
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

$app->get('/googleb8c1a22a15006829.html', function(Application $app, Request $request){
    return 'google-site-verification: googleb8c1a22a15006829.html';
});

$app->get('/robots.txt', function(Application $app, Request $request){
    return "User-agent: *"
            . PHP_EOL . "Allow: /"
            . PHP_EOL . "Disallow: /zn"
            . PHP_EOL . ""
            . PHP_EOL . "user-agent: AhrefsBot"
            . PHP_EOL . "disallow: /";
});