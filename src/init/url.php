<?php
use Silex\Provider\UrlGeneratorServiceProvider;

$app->register(new UrlGeneratorServiceProvider());