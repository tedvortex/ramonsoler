<?php
$link = $view['slots']->get('link');
$routes = $view['slots']->get('routes');
$view->extend('layout/desktop.php'); ?>

<div class="main-container">
    <div class="well product-description">
        <div class="row">
            <div class="col-xs-12 col-md-5">
                <div class="product-gallery">
                    <h1><?php echo $name ?></h1>
                    <?php if ($images !== false) {?>
                    <div id="image-carousel" class="carousel slide">
                        <div class="carousel-inner">
                            <?php foreach ($images as $k => $item) {?>
                            <a rel="product-gallery" onclick="return false;" href="<?php echo $view['assets']->getUrl('i/imagini-produse/' . $item['image']) ?>" class="lightbox item<?php echo ($k == 0 ? ' active' : '') ?>">
                                <img src="<?php echo $view['assets']->getUrl('i/imagini-produse/' . $item['image']) ?>" alt="<?php echo $name ?> - poza #<?php echo $k + 1 ?>"/>
                            </a>
                            <?php }?>
                        </div>
                    </div>
                    <?php }?>
                </div>
                <?php if ($simmilar) {?>
                <div id="simmilar">
                    <h3>Descopera produse similare</h3>
                    <div class="row">
                        <?php foreach ($simmilar as $item) {?>
                        <div class="col-xs-6 col-md-3 text-center">
                            <a class="image" href="<?php echo $item['link'] ?>" title="<?php echo $item['name'] ?>">
                                <img class="img-responsive" src="<?php echo '/resize/100?folder=/public/i/imagini-produse&image=' . $item['images'][0]['image'] ?>" alt="<?php echo $name ?> - poza #<?php echo $k + 1 ?>"/>
                            </a>
                            <a class="btn btn-black" href="<?php echo $item['link'] ?>"><?php echo $item['code'] ?></a>
                        </div>
                        <?php }?>
                    </div>
                </div>
                <?php }?>
            </div>
            <div class="col-xs-12 col-md-7">
                <div id="product-tabbed-description">
                    <ul class="row nav nav-tabs">
                        <li class="col-md-4 active"><a href="#specifications" data-toggle="tab" rel="nofollow">Specificatii tehnice</a></li>
                        <li class="col-md-4"><a href="#technology" data-toggle="tab" rel="nofollow">Tehnologii</a></li>
                        <li class="col-md-4"><a href="#design" data-toggle="tab" rel="nofollow">Schita tehnica</a></li>
                    </ul>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="specifications"><?php echo $specifications ?></div>
                                    <div class="tab-pane" id="technology">
                                        <?php if ($technologies !== false) {?>
                                        <ul class="technology-list clearfix">
                                            <?php foreach ($technologies as $t) {?>
                                            <li class="pull-left">
                                                <img class="tooltip-trigger"
                                                     data-original-title="<?php echo $t['description'] ?>"
                                                     data-container="body"
                                                     data-toggle="tooltip"
                                                     data-placement="bottom"
                                                     alt="<?php echo $t['name'] ?>" src="<?php echo $view['assets']->getUrl('i/imagini-tehnologii/' . $t['image']) ?>" />
                                            </li>
                                            <?php }?>
                                        </ul>
                                        <?php }?>
                                    </div>
                                    <div class="tab-pane" id="design">
                                        <?php if ($pdf) {?>
                                        <a class="pdf-link" href="<?php echo $view['assets']->getUrl('pdf-produse/' . $pdf) ?>" title="Vizualizeaza PDF">Vizualizeaza PDF</a>
                                        <?php } else {?>
                                        <p>Acest produs nu are schita tehnica !</p>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <?php if ($stock > 0) {?>
                                <div class="stock-info">
                                    <strong>Produsul se afla in stoc</strong>
                                    <div class="description clearfix">
                                        <p>Termen de livrare:</p>
                                        <p>intre 3-10 zile calendaristice</p>
                                        <?php if (isset($routes['buy'])) {
                                            $p = $routes['buy']; ?>
                                        <a class="bottom-display pull-right" href="<?php echo $p['route'] ?>" title="<?php echo $p['name'] ?>"><?php echo $p['name'] ?></a>
                                        <?php }?>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $view->render('widget/share.php'); ?>
                <div class="row text-center" id="product-shopcart-options">
                    <span class="btn btn-black">Pret: <?php echo $price . ' ' . $currency ?></span>
                    <span class="btn btn-darkblack">Cantitate: <input type="number" value="1" id="product-quantity"/></span>
                    <a href="#" class="btn btn-red add-to-cart" data-id="<?php echo $id_product ?>" data-quantity="#product-quantity">Adauga in cos</a>
                </div>
            </div>
        </div>
    </div>
</div>
