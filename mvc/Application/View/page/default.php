<?php
$link = $view['slots']->get('link');
$view->extend('layout/desktop.php'); ?>

<div class="main-container">
    <div class="well">
        <div class="content-hgroup">
            <h1><?php echo $name ?><span class="triangle"></span></h1>
        </div>
        <div class="page-description"><?php echo $description ?></div>
        <br/>
        <?php echo $view->render('widget/share.php'); ?>
    </div>
</div>