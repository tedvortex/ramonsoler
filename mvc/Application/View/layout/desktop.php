<?php
$routes = $view['slots']->get('routes');
$footerPages = $view['slots']->get('footerPages');
$boxPages = $view['slots']->get('boxPages');
$link = $view['slots']->get('link');
$categories = $view['slots']->get('categories');
$user = $view['slots']->get('_user');
foreach ($categories as $k => $c) {
    $c['is_active'] = false;

    if ($c['subcategories'] !== false) {
        if ($c['link'] == $link) {
            $c['is_active'] = true;
        }

        foreach ($c['subcategories'] as $kk => $sc) {
            $sc['is_active'] = false;

            if (stripos($link, $sc['link']) !== false) {
                $c['is_active'] = true;
                $sc['is_active'] = true;
            }

            $c['subcategories'][$kk] = $sc;
        }
    }

    $categories[$k] = $c;
}
$categoryCollapsed = $view['slots']->get('categoryCollapsed');
$productCount = $view['slots']->get('productCount');
?><!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml"
      lang="<?php $view['slots']->output('language', 'ro') ?>"
      xml:lang="<?php $view['slots']->output('language', 'ro') ?>">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name=viewport content="width=device-width, initial-scale=1" />

        <title><?php $view['slots']->output('title', 'Default title') ?></title>
        <meta name="keywords" content="<?php $view['slots']->output('meta_keywords', 'Default keywords') ?>"/>
        <meta name="description" content="<?php $view['slots']->output('meta_description', 'Default description') ?>"/>

        <link rel="stylesheet"    href="<?php echo $view['assets']->getUrl('css/bootstrap.min.css') ?> "/>
        <link rel="stylesheet"    href="<?php echo $view['assets']->getUrl('css/style.css') ?>" />
        <link rel="shortcut icon" href="<?php echo $view['assets']->getUrl('image/favicon.ico') ?>" type="image/x-icon" />
        <link rel="icon"          href="<?php echo $view['assets']->getUrl('image/favicon.ico') ?>" type="image/x-icon" />
    </head>
    <body>
        <?php
        $sizes = array('xs', 'sm', 'md', 'lg');

        foreach ($sizes as $size) {?>
        <div id="background-carousel-<?php echo $size ?>" class="background-carousel carousel slide visible-<?php echo $size ?>" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <?php for ($i = 1; $i < 6; $i++) {?>
                <div class="item<?php echo ($i === 1 ? ' active' : '') ?>">
                    <img class="img-responsive" src="<?php echo $view['assets']->getUrl('image/banner/' . $size . '/' . $i . '.jpg') ?>" alt="Banner slide #<?php echo $i ?>"/>
                </div>
                <?php }?>
            </div>
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php for ($i = 0; $i < 5; $i++) {?>
                <li data-target="#background-carousel-<?php echo $size ?>" data-slide-to="<?php echo $i ?>" <?php echo ($i === 0 ? 'class="active"' : '') ?>></li>
                <?php }?>
            </ol>
        </div>
        <?php }?>
        <section id="wrapper">
            <header id="header">
                <div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-header">
                                <span class="sr-only">Deschide navigatia</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-header">
                            <ul class="nav navbar-nav navbar-right">
                                <?php if (null === $user || ! $user) {
                                    if (isset($routes['login'])) {
                                        $p = $routes['login']; ?>
                                    <li><a href="<?php echo $p['route'] ?>" title="<?php echo $p['name'] ?>"><?php echo $p['name'] ?></a></li>
                                    <?php }

                                    if (isset($routes['register'])) {
                                        $p = $routes['register']; ?>
                                    <li><a href="<?php echo $p['route'] ?>" title="<?php echo $p['name'] ?>"><?php echo $p['name'] ?></a></li>
                                    <?php }
                                } else {
                                    if (isset($routes['account'])) {
                                        $p = $routes['account']; ?>
                                    <li><a href="<?php echo $p['route'] ?>" title="<?php echo $p['name'] ?>"><?php echo $p['name'] ?></a></li>
                                    <li><a href="<?php echo $p['route'] . '/logout' ?>" title="Logout">Logout</a></li>
                                    <?php }
                                } ?>

                                <li>Call center: <?php $view['slots']->output('header.call') ?></li>

                                <li class="divider"></li>
                                <li class="pull-right" id="header-shopcart-status"><?php echo ($productCount > 0 ? 'Aveti ' . $productCount . ' produs' . ($productCount > 1 ? 'e' : '') . ' in cos' : 'Nu aveti niciun produs in cos !') ?></li>
                                <li class="pull-right">
                                    <div id="header-search">
                                        <input type="text" placeholder="Cauta produs" value=""/>
                                        <a href="#" class="sprite header-search-icon" rel="nofollow"></a>
                                    </div>
                                </li>
                                <li id="header-shopcart-basket" class="dropdown">
                                    <ul id="header-shopcart-products-list" class="dropdown-menu">
                                        <?php if (isset($routes['cart'])) {
                                            $p = $routes['cart']; ?>
                                        <li><a href="<?php echo $p['route'] ?>" title="<?php echo $p['name'] ?>"><?php echo $p['name'] ?></a></li>
                                        <?php }?>
                                    </ul>
                                    <a data-toggle="dropdown" href="#" class="sprite header-basket-icon" rel="nofollow"></a>
                                    <div class="background"></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
            <section id="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-md-3">
                            <a id="logo" href="<?php echo $routes['homepage']['route'] ?>" title="RamonSoler.ro - Home">
                                <img class="img-responsive" src="<?php echo $view['assets']->getUrl('image/logo.png') ?>" alt="RamonSoler.ro Logo"/>
                            </a>
                            <?php if ($boxPages) {?>
                            <ul id="box-navigation">
                                <?php foreach ($boxPages as $p) {?>
                                <li>
                                    <?php if ($p['route_name'] == 'categories') {?>
                                    <a  href="#" title="<?php echo $p['name'] ?>" rel="nofollow"><?php echo $p['name'] ?></a>
                                    <?php if ($categories) {?>
                                    <ul id="categories-dropdown">
                                        <?php foreach ($categories as $c) {?>
                                        <li>
                                            <a href="<?php echo $c['link'] ?>" title="<?php echo $c['name'] ?>"><?php echo $c['name'] ?></a>
                                            <?php if ($c['has_children']) {?>
                                            <ul id="subcategories-dropdown-<?php echo $c['id_category'] ?>" class="collapsed">
                                                <?php foreach ($c['subcategories'] as $sc) {?>
                                                <li><a <?php echo ($sc['is_active'] ? 'class="active"' : '') ?> href="<?php echo $sc['link'] ?>" title="<?php echo $sc['name'] ?>"><?php echo $sc['name'] ?></a></li>
                                                <?php }?>
                                            </ul>
                                            <?php }?>
                                        </li>
                                        <?php }?>
                                    </ul>
                                    <?php }?>
                                    <?php } else {?>
                                    <a href="<?php echo $p['route'] ?>" title="<?php echo $p['name'] ?>"><?php echo $p['name'] ?></a>
                                    <?php }?>
                                </li>
                                <?php }?>
                            </ul>
                            <?php }?>
                        </div>
                        <div class="col-xs-12 col-md-9"><?php $view['slots']->output('_content') ?></div>
                    </div>
                </div>
            </section>
        </section>
        <footer id="footer">
            <div class="navbar navbar-inverse" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-footer">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-footer">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-footer">
                        <?php if ($footerPages) {?>
                        <ul class="nav navbar-nav navbar-right">
                            <?php foreach ($footerPages as $p) {?>
                            <li role="presentation">|</li>
                            <li><a href="<?php echo $p['route'] ?>" title="<?php echo $p['name'] ?>"><?php echo $p['name'] ?></a></li>
                            <?php }?>
                        </ul>
                        <?php }?>
                    </div>
                </div>
            </div>
        </footer>
        <div id="scripts">
            <script data-main="<?php echo $view['assets']->getUrl('javascript/default.js') ?>">
                var parentElement = document.getElementById('scripts'),
                    theFirstChild = parentElement.firstChild;
                    node = document.createElement('script');
                    node.type = 'text/javascript';
                    node.async = true;
                    node.src = '<?php echo $view['assets']->getUrl('javascript/lib/require-2.1.9.min.js') ?>';
                    parentElement.insertBefore(node, theFirstChild);
            </script>
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', '<?php $view['slots']->output('ga._setAccount') ?>', '<?php $view['slots']->output('ga._setDomainName') ?>');
                ga('send', 'pageview');
            </script>
        </div>
    </body>
</html>