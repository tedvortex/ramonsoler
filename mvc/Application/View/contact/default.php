<?php
$link = $view['slots']->get('link');
$view->extend('layout/desktop.php'); ?>

<div class="main-container">
    <div class="well">
        <div class="content-hgroup">
            <h1><?php echo $name ?><span class="triangle"></span></h1>
        </div>
        <div class="container-fluid" id="contact-headline">
            <p>Pentru nelamuriri referitoare la comanda sau informatii referitoare la produsele noastre, completeaza formularul demai jos, iar noi te vom contacta in cel mai scurt timp posibil.</p>
        </div>
        <div class="container-fluid page-description">
            <div class="row">
                <div class="col-md-6 form-main-container">
                    <form method="post" action class="bs-example form-horizontal" enctype="multipart/form-data">
                        <fieldset>
                            <?php echo $view['form']->widget($form); ?>
                        </fieldset>
                    </form>
                </div>
                <div class="col-md-6">
                    <?php echo $description ?>
                    <div id="map">
                        <iframe width="" height="" frameborder="0" scrolling="no"
                                marginheight="0" marginwidth="0"
                                src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ro&amp;geocode=&amp;q=Calea+Victoriei,+Bucure%C8%99ti,+Bucure%C5%9Fti,+Rom%C3%A2nia+201&amp;aq=&amp;sll=44.441989,26.094226&amp;sspn=0.024144,0.063944&amp;ie=UTF8&amp;hq=&amp;hnear=Calea+Victoriei+201,+Sector+1,+Bucure%C8%99ti,+Ilfov,+Rom%C3%A2nia&amp;t=m&amp;ll=44.443095,26.090813&amp;spn=0.014584,0.031672&amp;z=14&amp;output=embed"
                            ></iframe>
                        <br />
                        <small>
                            <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=ro&amp;geocode=&amp;q=Calea+Victoriei,+Bucure%C8%99ti,+Bucure%C5%9Fti,+Rom%C3%A2nia+201&amp;aq=&amp;sll=44.440989,26.094226&amp;sspn=0.024144,0.063944&amp;ie=UTF8&amp;hq=&amp;hnear=Calea+Victoriei+201,+Sector+1,+Bucure%C8%99ti,+Ilfov,+Rom%C3%A2nia&amp;t=m&amp;ll=44.443095,26.090813&amp;spn=0.014584,0.031672&amp;z=12" style="color:#0000FF;text-align:left">View zoomed map</a>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>