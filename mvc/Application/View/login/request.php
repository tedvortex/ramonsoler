<?php $view->extend('layout/desktop.php'); ?>

<div class="row">
    <div class="well form-main-container">
        <div class="form-main-container alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Success !</strong> Cererea de resetare a parolei a fost primita !<br/>Veti primi in scurt timp pe mail instructiuni pentru resetarea parolei !
        </div>
    </div>
</div>