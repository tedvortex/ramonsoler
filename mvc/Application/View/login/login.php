<?php
$routes = $view['slots']->get('routes');
$link = $view['slots']->get('link');
$view->extend('layout/desktop.php'); ?>

<div class="text-center">
<div class="main-container" id="login-wrapper">
    <div class="well">
        <div class="content-hgroup">
            <h1><?php echo $name ?><span class="triangle"></span></h1>
        </div>
        <div class="page-description text-left">
            <form method="post" action="<?php echo $action ?>" class="bs-example form-horizontal" enctype="multipart/form-data">
                <fieldset>
                    <div id="form">
                        <div class="form-error"><?php echo $error ?></div>
                        <div class="form-group">
                            <label for="_username" class="col-lg-3 control-label required">Email</label>
                            <div class="col-lg-9">
                                <input type="text" placeholder="email@site.com" class="form-control" required="required" name="_username" id="_username" value="<?php echo $last_username ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="form__password" class="col-lg-3 control-label required">Parola</label>
                            <div class="col-lg-9">
                                <input type="password" placeholder="******" class="form-control" required="required" name="_password" id="_password"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-9 col-lg-offset-3">
                                <button class="btn btn-primary" name="send" id="send" type="submit">Autentificare</button>
                                <?php if (isset($routes['forgot'])) {
                                    $p = $routes['forgot']; ?>
                                    <a class="btn btn-danger pull-right" href="<?php echo $p['route'] ?>" title="<?php echo $p['name'] ?>"><?php echo $p['name'] ?></a>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
</div>