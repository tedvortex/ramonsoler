<?php $view->extend('layout/desktop.php'); ?>

<div class="row">
    <div class="well form-main-container">
        <div class="form-main-container alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Eroare !</strong> <?php echo $reset_message ?>
        </div>
    </div>
</div>