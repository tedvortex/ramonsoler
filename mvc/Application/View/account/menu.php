<?php if ($routes) {?>
<ul class="account-menu">
    <?php foreach (array('profile', 'history') as $route) {
        if (isset($routes[$route])) {
            $p = $routes[$route]; ?>
    <li><a href="<?php echo $p['route'] ?>" title="<?php echo $p['name'] ?>"><?php echo $p['name'] ?></a></li>
        <?php }
     }?>
</ul>
<?php }?>