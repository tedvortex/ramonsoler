<?php
$link = $view['slots']->get('link');
$routes = $view['slots']->get('routes');
$view->extend('layout/desktop.php'); ?>

<div class="main-container">
    <div class="well">
        <div class="content-hgroup">
            <h1><?php echo $name ?><span class="triangle"></span></h1>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-md-3">
                    <?php echo $view->render('account/menu.php', array('routes' => $routes)); ?>
                </div>
                <div class="col-xs-12 col-md-9">
                    <div class="page-description"><?php echo $description ?></div>
                </div>
            </div>
        </div>
    </div>
</div>