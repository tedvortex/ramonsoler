<?php $view->extend('layout/desktop.php'); ?>

<div class="row">
    <div class="well form-main-container">
        <div class="form-main-container alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Success !</strong> Contul dumneavoastra a fost activat cu succes !
        </div>
    </div>
</div>