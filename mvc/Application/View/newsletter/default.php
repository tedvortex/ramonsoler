<?php
$link = $view['slots']->get('link');
$view->extend('layout/desktop.php'); ?>

<div class="text-center">
<div class="main-container" id="login-wrapper">
    <div class="well">
        <div class="content-hgroup">
            <h1><?php echo $name ?><span class="triangle"></span></h1>
        </div>
        <div class="page-description text-left">
            <form method="post" action class="bs-example form-horizontal" enctype="multipart/form-data">
                <fieldset>
                    <?php echo $view['form']->widget($form); ?>
                </fieldset>
            </form>
        </div>
    </div>
</div>
</div>