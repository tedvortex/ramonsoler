<?php
$link = $view['slots']->get('link');
$view->extend('layout/desktop.php'); ?>

<div class="main-container">
    <div class="well">
        <div class="content-hgroup">
            <h1><?php echo $name ?><span class="triangle"></span></h1>
        </div>
        <div class="page-description">
            <div class="row">
                <div class="col-xs-12 col-md-4 form-main-container register-collapsable" id="form-fizica">
                    <form method="post" action class="bs-example form-horizontal" enctype="multipart/form-data">
                        <h2 class="form-header"><input type="radio" name="person" value="form-fizica" class="register-collapser" data-checked="true" autocomplete="false"/> Persoana fizica</h2>
                        <fieldset>
                            <h3 class="form-header">Informatii personale</h3>
                            <?php echo $view['form']->widget($form); ?>
                        </fieldset>
                    </form>
                </div>
                <form method="post" action class="bs-example form-horizontal register-collapsable gray" enctype="multipart/form-data" id="form-juridica">
                    <div class="col-xs-12 col-md-4 form-main-container">
                        <h2 class="form-header"><input type="radio" name="person" value="form-juridica" class="register-collapser" autocomplete="false"/> Persoana juridica / companie</h2>
                        <fieldset>
                            <h3 class="form-header">Informatii firma</h3>
                            <?php echo $view['form']->widget($form2); ?>
                        </fieldset>
                    </div>
                    <div class="col-xs-12 col-md-4 form-main-container">
                        <h2 class="form-header" style="visibility: hidden;">Persoana juridica / companie</h2>
                        <fieldset>
                            <h3 class="form-header">Persoana de contact</h3>
                            <?php echo $view['form']->widget($form3); ?>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>