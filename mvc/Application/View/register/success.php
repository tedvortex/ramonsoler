<?php $view->extend('layout/desktop.php'); ?>

<div class="row">
    <div class="well form-main-container">
        <div class="form-main-container alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Success !</strong> Contul dumneavoastra a fost inregistrat cu success ! Va rugam verificati casuta de email pentru instructiuni de activare !
        </div>
    </div>
</div>