<?php
$routes = $view['slots']->get('routes');
$link = $view['slots']->get('link');
$view->extend('layout/desktop.php'); ?>

<div class="main-container">
    <div class="well">
        <div class="content-hgroup">
            <h1><?php echo $name ?><span class="triangle"></span></h1>
        </div>
        <div class="page-description">
            <div class="row">
                <div class="col-md-12 shopcart-display" id="shopcart">
                    <?php if (is_array($cart) && ! empty($cart)) {?>
                    <ul class="list-shopcart">
                        <li class="container-fluid heading">
                            <div class="row">
                                <div class="col-md-2 text-center">Imagine</div>
                                <div class="col-md-4">Nume produs</div>
                                <div class="col-md-2 text-center">Cantitate</div>
                                <div class="col-md-2 text-center">Sterge</div>
                                <div class="col-md-2 text-center">Pret</div>
                            </div>
                        </li>
                        <?php foreach ($cart as $key => $product) {?>
                        <li class="container-fluid" id="shopcart-item-<?php echo $key ?>">
                            <div class="row">
                                <div class="col-md-2 text-center">
                                    <a href="<?php echo $product['link'] ?>" title="<?php echo $product['name'] ?>">
                                        <img src="<?php echo '/resize/100?folder=' . $product['folder'] . '&amp;image=' . $product['image'] ?>" alt=""/>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="link" href="<?php echo $product['link'] ?>" title="<?php echo $product['name'] ?>"><?php echo $product['name'] ?></a>
                                </div>
                                <div class="col-md-2 text-center"><input type="number" name="update-cart[]" data-id="<?php echo $key ?>" data-price="<?php echo $product['price'] ?>" data-target="#shopcart-item-<?php echo $key ?> .price" class="update-cart" value="<?php echo $product['quantity'] ?>" autocomplete="off"/></div>
                                <div class="col-md-2 text-center"><a class="delete-from-cart" href="#" data-target="#shopcart-item-<?php echo $key ?>" data-id="<?php echo $key ?>">sterge din cos</a></div>
                                <div class="col-md-2 text-center"><span class="price"><?php echo $product['price'] * $product['quantity'] ?></span> RON</div>
                            </div>
                        </li>
                        <?php }?>
                    </ul>
                    <div class="container-fluid" id="shopcart-summary">
                        <div class="row">
                            <div class="col-md-10 text-right">Total produse: </div>
                            <div class="col-md-2" id="shopcart-total-products"><?php echo $count ?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 text-right">Total pret: </div>
                            <div class="col-md-2"><span id="shopcart-total-price"><?php echo $total ?></span> RON</div>
                        </div>
                    </div>
                    <p class="no-items hidden">Nu aveti produse in cos !</p>
                    <?php } else {
                        echo '<p class="no-items">Nu aveti produse in cos !</p>';
                    } ?>
                </div>
            </div>
            <br/>
            <?php if (is_array($cart) && ! empty($cart)) {?>
            <div class="row" id="order">
                <div class="col-xs-12 col-md-8">
                    <div class="shopcart-collapsable">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="bs-example form-horizontal">
                                    <div class="form-main-container">
                                        <h2 class="form-header">Doresc facturarea comenzii pe:</h2>
                                        <fieldset>
                                            <div id="order-person-select">
                                                <div class="form-group">
                                                    <input class="person-select" type="radio" name="person-select" value="fizica" id="person-select-fizica" checked="checked" autocomplete="off"
                                                           data-class=".form-collapsable"
                                                        data-target="#form-fizica-container"/> <label for="person-select-fizica">PERSOANA FIZICA</label><br/>
                                                    <input class="person-select" type="radio" name="person-select" value="juridica" id="person-select-juridica" autocomplete="off"
                                                           data-class=".form-collapsable"
                                                        data-target="#form-juridica-container"/> <label for="person-select-juridica">PERSOANA JURIDICA</label>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-collapsable" id="form-fizica-container">
                            <form method="post" action class="bs-example form-horizontal" enctype="multipart/form-data" id="form-shopcart-fizica">
                                <div class="col-md-12 form-main-container">
                                    <input type="hidden" name="step" value="order" />
                                    <input type="hidden" name="person" value="fizica"/>
                                    <h2 class="form-header">Doresc sa mi se livreze comanda la:</h2>
                                    <fieldset>
                                        <?php echo $view['form']->widget($form); ?>
                                    </fieldset>
                                    <h2 class="form-header">Adresa:</h2>
                                    <fieldset>
                                        <?php echo $view['form']->widget($form2); ?>
                                    </fieldset>
                                    <h2 class="form-header">Doresc sa-mi confirmati comanda la:</h2>
                                    <fieldset>
                                        <?php echo $view['form']->widget($form3); ?>
                                    </fieldset>
                                    <h2 class="form-header">Observatii:</h2>
                                    <fieldset>
                                        <?php echo $view['form']->widget($form4); ?>
                                    </fieldset>
                                </div>
                            </form>
                        </div>
                        <div class="row form-collapsable hidden" id="form-juridica-container">
                            <form method="post" action class="bs-example form-horizontal" enctype="multipart/form-data" id="form-shopcart-juridica">
                                <div class="col-md-12 form-main-container">
                                    <input type="hidden" name="step" value="order" />
                                    <input type="hidden" name="person" value="juridica"/>
                                    <h2 class="form-header">Doresc factura cu datele firmei:</h2>
                                    <fieldset>
                                        <?php echo $view['form']->widget($form_account); ?>
                                    </fieldset>
                                    <h2 class="form-header">Doresc sa mi se livreze comanda la:</h2>
                                    <fieldset>
                                        <?php echo $view['form']->widget($form_account2); ?>
                                    </fieldset>
                                    <h2 class="form-header">Adresa:</h2>
                                    <fieldset>
                                        <?php echo $view['form']->widget($form_account3); ?>
                                    </fieldset>
                                    <h2 class="form-header">Doresc sa-mi confirmati comanda la:</h2>
                                    <fieldset>
                                        <?php echo $view['form']->widget($form_account4); ?>
                                    </fieldset>
                                    <h2 class="form-header">Observatii:</h2>
                                    <fieldset>
                                        <?php echo $view['form']->widget($form_account5); ?>
                                    </fieldset>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 form-main-container" id="form-login">
                    <div class="shopcart-collapsable">
                        <form method="post" action class="bs-example form-horizontal" enctype="multipart/form-data">
                            <h2 class="form-header">Login</h2>
                            <fieldset>
                                <input type="hidden" name="step" value="login" />
                                <input type="hidden" name="_target_path" value="<?php echo $link ?>" />
                                <div class="form-error"><?php echo $error ?></div>
                                <div class="form-group">
                                    <label for="_username" class="col-lg-3 control-label required">Email</label>
                                    <div class="col-lg-9">
                                        <input type="text" placeholder="email@site.com" class="form-control" required="required" name="_username" id="_username" value="<?php echo $last_username ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="form__password" class="col-lg-3 control-label required">Parola</label>
                                    <div class="col-lg-9">
                                        <input type="password" placeholder="******" class="form-control" required="required" name="_password" id="_password"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-9 col-lg-offset-3">
                                        <button class="btn btn-primary" name="send" id="send" type="submit">Autentificare</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</div>