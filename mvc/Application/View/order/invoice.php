<?php
$routes = $view['slots']->get('routes');
$link = $view['slots']->get('link');
$view->extend('layout/desktop.php'); ?>

<div class="main-container">
    <div class="well">
        <div class="content-hgroup">
            <h1>Finalizare comanda<span class="triangle"></span></h1>
        </div>
        <div class="page-description">
            <div class="row">
                <div class="col-md-12 shopcart-display" id="shopcart">
                    <?php if (is_array($cart) && ! empty($cart)) {?>
                        <ul class="list-shopcart list-invoice">
                            <li class="container-fluid heading">
                                <div class="row">
                                    <div class="col-md-2 text-center">Imagine</div>
                                    <div class="col-md-6">Nume produs</div>
                                    <div class="col-md-2 text-center">Cantitate</div>
                                    <div class="col-md-2 text-center">Pret</div>
                                </div>
                            </li>
                            <?php foreach ($cart as $key => $product) {?>
                                <li class="container-fluid" id="shopcart-item-<?php echo $key ?>">
                                    <div class="row">
                                        <div class="col-md-2 text-center">
                                            <a href="<?php echo $product['link'] ?>" title="<?php echo $product['name'] ?>">
                                                <img src="<?php echo '/resize/100?folder=' . $product['folder'] . '&amp;image=' . $product['image'] ?>" alt=""/>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a class="link" href="<?php echo $product['link'] ?>" title="<?php echo $product['name'] ?>"><?php echo $product['name'] ?></a>
                                        </div>
                                        <div class="col-md-2 text-center"><input type="number" name="update-cart[]" data-id="<?php echo $key ?>" data-price="<?php echo $product['price'] ?>" data-target="#shopcart-item-<?php echo $key ?> .price" class="update-cart" value="<?php echo $product['quantity'] ?>" autocomplete="off" disabled="disabled"/></div>
                                        <div class="col-md-2 text-center"><span class="price"><?php echo $product['price'] * $product['quantity'] ?></span> RON</div>
                                    </div>
                                </li>
                            <?php }?>
                        </ul>
                        <?php if ($order) {?>
                        <div class="shopcart-collapsable" id="order-summary">
                            <div class="form-horizontal">
                                <?php if ($order['type']) {?>
                                <h2 class="form-header">Date firma:</h2>
                                <div class="invoice-bottom-padded">
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">Nume firma:</div>
                                            <div class="col-xs-6 col-md-9"><?php echo $order['company_name'] ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">CUI:</div>
                                            <div class="col-xs-6 col-md-9"><?php echo $order['company_cui'] ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">NR REG COM:</div>
                                            <div class="col-xs-6 col-md-9"><?php echo $order['company_reg'] ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">IBAN:</div>
                                            <div class="col-xs-6 col-md-9"><?php echo $order['company_iban'] ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">BANCA:</div>
                                            <div class="col-xs-6 col-md-9"><?php echo $order['company_bank'] ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">ADRESA:</div>
                                            <div class="col-xs-6 col-md-9"><?php echo $order['company_address'] ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">TELEFON:</div>
                                            <div class="col-xs-6 col-md-9"><?php echo $order['company_phone'] ?></div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                <h2 class="form-header">Cumparator:</h2>
                                <div class="invoice-bottom-padded">
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">Nume:</div>
                                            <div class="col-xs-6 col-md-9"><?php echo $order['first_name'] ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">Prenume:</div>
                                            <div class="col-xs-6 col-md-9"><?php echo $order['last_name'] ?></div>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="form-header">Adresa de livrare:</h2>
                                <div class="invoice-bottom-padded">
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">Strada:</div>
                                            <div class="col-xs-6 col-md-9"><?php echo $order['address'] ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">Judet:</div>
                                            <div class="col-xs-6 col-md-9"><?php echo $order['state'] ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">Oras:</div>
                                            <div class="col-xs-6 col-md-9"><?php echo $order['city'] ?></div>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="form-header">Plata:</h2>
                                <div class="invoice-bottom-padded">
                                    <div class="form-group container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 col-md-3">Modalitate plata:</div>
                                            <div class="col-xs-6 col-md-9">Plata la livrare</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="total">
                                <div class="row">
                                    <div class="col-xs-6 col-md-10 text-right">Total produse: </div>
                                    <div class="col-xs-6 col-md-2" id="shopcart-total-products"><?php echo $count ?></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 col-md-10 text-right">Total pret: </div>
                                    <div class="col-xs-6 col-md-2"><span id="shopcart-total-price"><?php echo $total ?></span> RON</div>
                                </div>
                            </div>
                        </div>
                        <form method="post" action>
                            <div id="order-final" class="text-center">
                                <input type="hidden" name="step" value="invoice" />
                                <input type="hidden" name="invoice" value="<?php echo $token ?>" />
                                <p>Daca toate datele comenzii sunt corecte si valabile atunci puteti incheia comanda  apasand butonul de mai jos:</p>
                                <button type="submit" class="btn btn-danger btn-lg">COMANDA</button>
                            </div>
                        </form>
                        <?php }
                    } else {
                        // hack attempt
                    }?>
                </div>
            </div>
            <br/>
        </div>
    </div>
</div>