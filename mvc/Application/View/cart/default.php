<?php
$routes = $view['slots']->get('routes');
$link = $view['slots']->get('link');
$view->extend('layout/desktop.php'); ?>

<div class="main-container">
    <div class="well">
        <div class="content-hgroup">
            <h1><?php echo $name ?><span class="triangle"></span></h1>
        </div>
        <div class="page-description">
            <div class="row">
                <div class="col-md-12 shopcart-display">
                    <?php if (is_array($cart) && ! empty($cart)) {?>
                    <ul class="list-shopcart">
                        <li class="container-fluid heading">
                            <div class="row">
                                <div class="col-md-2 text-center">Imagine</div>
                                <div class="col-md-4">Nume produs</div>
                                <div class="col-md-2 text-center">Cantitate</div>
                                <div class="col-md-2 text-center">Sterge</div>
                                <div class="col-md-2 text-center">Pret</div>
                            </div>
                        </li>
                        <?php foreach ($cart as $key => $product) {?>
                        <li class="container-fluid" id="shopcart-item-<?php echo $key ?>">
                            <div class="row">
                                <div class="col-md-2 text-center">
                                    <a href="<?php echo $product['link'] ?>" title="<?php echo $product['name'] ?>">
                                        <img src="<?php echo '/resize/100?folder=' . $product['folder'] . '&amp;image=' . $product['image'] ?>" alt=""/>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="link" href="<?php echo $product['link'] ?>" title="<?php echo $product['name'] ?>"><?php echo $product['name'] ?></a>
                                </div>
                                <div class="col-md-2 text-center"><input type="number" name="update-cart[]" data-id="<?php echo $key ?>" data-price="<?php echo $product['price'] ?>" data-target="#shopcart-item-<?php echo $key ?> .price" class="update-cart" value="<?php echo $product['quantity'] ?>" autocomplete="off"/></div>
                                <div class="col-md-2 text-center"><a class="delete-from-cart" href="#" data-target="#shopcart-item-<?php echo $key ?>" data-id="<?php echo $key ?>">sterge din cos</a></div>
                                <div class="col-md-2 text-center"><span class="price"><?php echo $product['price'] * $product['quantity'] ?></span> RON</div>
                            </div>
                        </li>
                        <?php }?>
                    </ul>
                    <div class="container-fluid" id="shopcart-summary">
                        <div class="row">
                            <div class="col-md-10 text-right">Total produse: </div>
                            <div class="col-md-2" id="shopcart-total-products"><?php echo $count ?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 text-right">Total pret: </div>
                            <div class="col-md-2"><span id="shopcart-total-price"><?php echo $total ?></span> RON</div>
                        </div>
                        <?php if (isset($routes['order'])) {
                            $p = $routes['order']; ?>
                        <div class="text-center">
                            <a class="btn btn-lg btn-danger" href="<?php echo $p['route'] ?>" title="<?php echo $p['name'] ?>"><?php echo $p['name'] ?></a>
                        </div>
                        <?php }?>
                    </div>
                    <p class="no-items hidden">Nu aveti produse in cos !</p>
                    <?php } else {
                        echo '<p class="no-items">Nu aveti produse in cos !</p>';
                    } ?>
                </div>
            </div>
        </div>
    </div>
</div>