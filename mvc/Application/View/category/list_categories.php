<?php
$link = $view['slots']->get('link');
$routes = $view['slots']->get('routes');
$view->extend('layout/desktop.php'); ?>

<div class="main-container">
    <div class="well">
        <div class="content-hgroup">
            <h1><?php echo $routes['categories']['name'] ?><span class="triangle"></span></h1>
            <h2><?php echo $name ?></h2>
        </div>
        <div class="subcategory-list row">
            <div class="col-md-12">
                <?php if ($subcategories !== false) {?>
                <ul class="list-categories row">
                    <?php foreach ($subcategories as $item) {?>
                    <li class="col-md-3">
                        <a class="img-wrapper" href="<?php echo $item['link'] ?>" title="<?php echo $item['name'] ?>">
                            <img class="img-responsive" src="<?php echo $view['assets']->getUrl('i/imagini-categorii/' . $item['image']) ?>" alt="<?php echo $item['name'] ?>"/>
                        </a>
                        <h3><a href="<?php echo $item['link'] ?>" title="<?php echo $item['name'] ?>"><?php echo $item['name'] ?></a></h3>
                    </li>
                    <?php }?>
                </ul>

                <br/>
                <?php echo $view->render('widget/share.php'); ?>
                <?php }?>
            </div>
        </div>
    </div>
</div>