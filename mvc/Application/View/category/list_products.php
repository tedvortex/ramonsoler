<?php
$link = $view['slots']->get('link');
$routes = $view['slots']->get('routes');
$view->extend('layout/desktop.php'); ?>

<div class="main-container">
    <div class="well">
        <div class="content-hgroup">
            <h1><?php echo $routes['categories']['name'] ?><span class="triangle"></span></h1>
            <h2><?php echo $name ?></h2>
        </div>
        <div class="category-description row">
            <?php if ($products) {?>
            <div class="col-xs-12 col-md-3">
                <?php if ($criteria) {?>
                <div class="list-criteria">
                    <h3 class="headline">Criterii de cautare:</h3>
                    <ul>
                    <?php foreach ($criteria as $c) {?>
                        <li>
                            <h3><?php echo ucfirst($c['name']) ?>:</h3>
                            <?php if ($c['option'] !== false) {?>
                            <ul class="option" id="options-criteria-<?php echo $c['id_criteria'] ?>">
                                <?php foreach ($c['option'] as $o) {?>
                                <li id="option-<?php echo $o['id_option'] ?>">
                                    <input type="checkbox" name="criteria-<?php echo $c['id_criteria'] ?>[]" id="criteria-<?php echo $o['id_option'] ?>" value="<?php echo $o['id_option'] ?>"/>
                                    <label for="criteria-<?php echo $o['id_option'] ?>"><?php echo $o['name'] ?></label>
                                </li>
                                <?php }?>
                            </ul>
                            <?php }?>
                        </li>
                    <?php }?>
                    </ul>
                </div>
                <?php }?>
            </div>

            <div class="col-xs-12 col-md-9">
                <ul class="list-products row">
                    <?php foreach ($products as $item) {?>
                    <li class="col-md-4">
                        <img class="background img-responsive" src="<?php echo $view['assets']->getUrl('image/product-background.png') ?>" alt="<?php echo $item['name'] ?>"/>
                        <h3><a href="<?php echo $item['link'] ?>" title="<?php echo $item['name'] ?>"><?php echo $item['code'] ?></a></h3>
                        <a class="img-wrapper" href="<?php echo $item['link'] ?>" title="<?php echo $item['name'] ?>">
                            <img class="img-responsive" src="<?php echo '/resize/200?folder=/public/i/imagini-produse&image=' . $item['images'][0]['image'] ?>" alt="<?php echo $item['name'] ?>"/>
                            <span class="price"><?php echo $item['price'] . ' ' . $item['currency'] ?></span>
                        </a>
                    </li>
                    <?php }?>
                </ul>
            </div>
            <?php } else {?>
            <div id="fourzerofour-description" class="page-description">Nu exista produse in aceasta categorie</div>
            <?php }?>
        </div>
    </div>
</div>