<?php
namespace Application\Model;

abstract class Singleton
{
    final public static function getInstance ($adapter) {
        static $instances = array();

        $calledClass = get_called_class();

        if (! isset($instances[$calledClass])) {
            $instances[$calledClass] = new $calledClass($adapter);
        }

        return $instances[$calledClass];
    }

    final private function __clone ()
    {}
}