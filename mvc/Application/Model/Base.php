<?php
namespace Application\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet;

abstract class Base extends Singleton
{
    public $adapter;
    public $table;
    public $identifier = 'id';
    public $prefixSeparator = '_';
    public $prefix = 'vortex';
    public $dataSuffix = 'data';
    public $columns = array(
        '*',
    );
    public $dataColumns = array(
        'name',
        'name_seo',
        'title',
        'meta_description',
        'meta_keywords',
        'description'
    );
    public $defaultSql;

    public function __construct ($adapter) {
        $this->adapter = $adapter;
    }

    public function url ($string, $replace = '-', $encode = true)
    {
        $string = mb_strtolower(mb_ereg_replace("[^a-zA-Z0-9]+", $replace, trim($string)));
        $string = mb_ereg_replace("{$replace}$", '', $string);
        $string = mb_ereg_replace("^{$replace}", '', $string);

        if ($encode) {
            $string = rawurlencode($string);
        }

        return $string;
    }

    public function link ($item) {
        return $this->url($item['name_seo']);
    }

    public function getSql ($extend = false) {
        $sql = new Sql($this->adapter);

        $convertedTable = $this->prefix . $this->prefixSeparator . $this->table;
        $convertedTableData = $convertedTable . $this->prefixSeparator . $this->dataSuffix;
        $convertedTableKey = mb_substr($this->table, 0, 1);
        $convertedTableDataKey = $convertedTableKey . mb_substr($this->dataSuffix, 0, 1);
        $id = $this->identifier . $this->prefixSeparator . $this->table;

        $select = $sql->select();
        $this->defaultSql = $select->from(array(
                                        mb_substr($this->table, 0, 1) => $convertedTable
                                     ))
                                   ->columns($this->columns)
                                   ->join(array(
                                            mb_substr($this->table, 0, 1) . mb_substr($this->dataSuffix, 0, 1) => $convertedTableData
                                         ),
                                         $convertedTableKey . '.' . $id . ' = ' . $convertedTableDataKey . '.' . $id,
                                         $this->dataColumns,
                                         $select::JOIN_INNER
                                     );

        $this->sql = $sql;

        if ($extend) {
            $this->getSqlExtend();
        }

        return $this->defaultSql;
    }

    public function table ($tableName = null) {
        return $this->prefix . $this->prefixSeparator . $tableName;
    }

    public function getSqlExtend () {
        // do stuff with $this->defaultSql;
    }

    public function hydrate ($result) {
        if ($result instanceof ResultInterface && $result->isQueryResult()) {
            $resultSet = new ResultSet;
            $resultSet->initialize($result);

            return $resultSet->toArray();
        }

        return false;
    }

    public function listAll ($where = null, $page = 1, $division = 0, $extendSql = false) {
        $select = $this->getSql($extendSql);

        if (! is_null($where)) {
            $select->where($where);
        }

        if ($division > 0) {
            $select->limit($division)
                   ->offset(($page - 1) * $division);
        }

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $this->hydrate($results);
    }

    public function listRow ($where = null, $extendSql = false) {
        $select = $this->getSql($extendSql);

        if (! is_null($where)) {
            $select->where($where);
        }

        $select->limit(1);

        $statement = $this->sql->prepareStatementForSqlObject($select);

        $result = $statement->execute();

        if ($result instanceof ResultInterface && $result->isQueryResult()) {
            $resultSet = new ResultSet;
            $resultSet->initialize($result);

            $results = $resultSet->toArray();

            if (is_array($results) && isset($results[0])) {
                return $results[0];
            }
        }

        return false;
    }

    public function listRowById ($id = 0, $extendSql = false) {
        return $this->listRow(array(
                mb_substr($this->table, 0, 1) . '.' . $this->identifier . $this->prefixSeparator . $this->table => $id
            ), $extendSql);
    }

    public function insert ($data) {
        $sql = new Sql($this->adapter);

        $convertedTable = $this->prefix . $this->prefixSeparator . $this->table;
        $convertedTableData = $convertedTable . $this->prefixSeparator . $this->dataSuffix;

        if (isset($data['convertedTable']) && is_array($data['convertedTable'])) {
            $insertTable = $sql->insert($convertedTable)
                               ->values($data['convertedTable']);

            $insertString = $sql->getSqlStringForSqlObject($insertTable);
            $statement = $this->adapter->query($insertString);
            $result = $statement->execute();

            $id = $result->getGeneratedValue();

            if (is_numeric($id)) {
                if (isset($data['convertedTableData']) && ! empty($data['convertedTableData'])) {
                    $data['convertedTableData']['id_' . $this->table] = $id;

                    $insertTableData = $sql->insert($convertedTableData)
                                           ->values($data['convertedTableData']);

                    $insertStringData = $sql->getSqlStringForSqlObject($insertTableData);
                    $statementData = $this->adapter->query($insertStringData);
                    $resultData = $statementData->execute();

                    $idData = $resultData->getGeneratedValue();

                    if (is_numeric($idData)) {
                        return $id;
                    } else {
                        return false;
                    }
                } else {
                    return $id;
                }
            } else {
                return false;
            }
        }

        return false;
    }

    public function update ($where, $data) {
        $return = array(
            'convertedTable' => false,
            'convertedTableData' => false,
        );

        $sql = new Sql($this->adapter);

        $convertedTable = $this->prefix . $this->prefixSeparator . $this->table;
        $convertedTableData = $convertedTable . $this->prefixSeparator . $this->dataSuffix;

        if (isset($data['convertedTable']) && is_array($data['convertedTable'])) {
            $updateTable = $sql->update($convertedTable)
                               ->set($data['convertedTable'])
                               ->where($where['convertedTable']);

            $updateString = $sql->getSqlStringForSqlObject($updateTable);
            $statement = $this->adapter->query($updateString);
            $result = $statement->execute();

            if ($result->count() > 0) {
                $return['convertedTable'] = true;
            }
        }

        if (isset($data['convertedTableData']) && is_array($data['convertedTableData'])) {
            $updateTableData = $sql->update($convertedTableData)
                                   ->set($data['convertedTableData'])
                                   ->where($where['convertedTableData']);

            $updateStringData = $sql->getSqlStringForSqlObject($updateTableData);
            $statement = $this->adapter->query($updateStringData);
            $result = $statement->execute();

            if ($result->count() > 0) {
                $return['convertedTableDataData'] = true;
            }
        }

        return $return;
    }
}