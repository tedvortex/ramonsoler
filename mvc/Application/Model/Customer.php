<?php
namespace Application\Model;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Zend\Db\Sql\Sql;

class Customer extends Base implements UserProviderInterface
{

    public $table = 'customer';

    public function loadUserByUsername($email)
    {
        $user = $this->listRow(array(
            'c.status' => 1,
            'c.email' => $email
        ));

        if ($user === false) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $email));
        }

        return new User($user['email'], $user['password'], explode(',', $user['role']), true, true, true, true);
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Symfony\Component\Security\Core\User\User';
    }

    public function getSql ($extend = false) {
        $sql = new Sql($this->adapter);

        $convertedTable = $this->prefix . $this->prefixSeparator . $this->table;
        $convertedTableKey = mb_substr($this->table, 0, 1);
        $id = $this->identifier . $this->prefixSeparator . $this->table;

        $select = $sql->select();
        $this->defaultSql = $select->from(array(
            mb_substr($this->table, 0, 1) => $convertedTable
        ))
        ->columns($this->columns);

        $this->sql = $sql;

        if ($extend) {
            $this->getSqlExtend();
        }

        return $this->defaultSql;
    }
}