<?php
namespace Application\Model;

class Technology extends Base
{

    public $table = 'technology';
    public $dataColumns = array(
        '*',
    );

    final public function findByClassName ($className = null)
    {
        return $this->listRow(array(
            't.status' => 1,
            'td.class_name' => $className
        ));
    }

    public function listAllByProduct ($id = null, $page = 1, $division = 0, $extendSql = true) {
        $select = $this->getSql($extendSql);

        if (is_numeric($id)) {
            $select->join(array(
                    'ptt' => 'vortex_product_to_technology'
                ),
                'ptt.id_technology = t.id_technology'
            )
            ->where(array(
                'ptt.id_product' => $id
            ));
        }

        if ($division > 0) {
            $select->limit($division)
                   ->offset(($page - 1) * $division);
        }

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $this->hydrate($results);
    }
}