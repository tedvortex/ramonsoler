<?php
namespace Application\Model;

class Email extends Base
{

    public $table = 'email';

    public $dataColumns = array(
        '*'
    );

    final public function listRowByTemplate ($name = null)
    {
        return $this->listRow(array(
            'e.status' => 1,
            'e.template' => $name
        ));
    }
}