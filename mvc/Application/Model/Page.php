<?php
namespace Application\Model;

class Page extends Base
{

    public $table = 'page';

    final public function findByNameSeo ($nameSeo = null)
    {
        return $this->listRow(array(
            'p.status' => 1,
            'pd.name_seo' => $nameSeo
        ));
    }
}