<?php
namespace Application\Model;

class Shopcart
{

    public $storage;

    private $options = array(
        'quantity',
        'name',
        'image',
        'folder',
        'price',
        'code',
        'link'
    );

    final public function __construct($storage)
    {
        $this->storage = $storage;
    }

    public function parseOptions($options)
    {
        $return = array();

        foreach ($this->options as $key) {
            if (isset($options[$key])) {
                $return[$key] = $options[$key];
            }
        }

        return $return;
    }

    public function get($id = 0, $return_boolean = true)
    {
        if (isset($this->storage[$id])) {
            return $return_boolean ? true : $this->storage[$id];
        }

        return false;
    }

    public function set($id = 0, $options = array())
    {
        $options = $this->parseOptions($options);

        $this->storage[$id] = $options;
    }

    public function add($id = 0, array $options = array())
    {
        $options = $this->parseOptions($options);

        if (false !== $item = $this->get($id, false)) {
            $item['quantity'] += $options['quantity'];
            $this->storage[$id] = $item;
        } else {
            $this->set($id, $options);

            return true;
        }

        return false;
    }

    public function update($id = 0, array $options = array())
    {
        if (false !== $item = $this->get($id, false)) {
            $options = $this->parseOptions($options);

            foreach ($options as $key => $option) {
                $item[$key] = $option;
            }

            $this->set($id, $item);

            return true;
        }

        return false;
    }

    public function remove($id = 0)
    {
        if ($this->get($id)) {
            unset($this->storage[$id]);

            return true;
        }

        return false;
    }

    public function count()
    {
        $return = 0;

        foreach ($this->storage as $product) {
            $return += $product['quantity'];
        }

        return $return;
    }

    public function total()
    {
        $return = 0;

        foreach ($this->storage as $product) {
            $return += $product['price'] * $product['quantity'];
        }

        return $return;
    }
}