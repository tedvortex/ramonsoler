<?php
namespace Application\Model;

use Zend\Db\Sql\Sql;

class Category extends Base
{

    public $table = 'category';

    public function getSqlExtend () {
        $sql = new Sql($this->adapter);

        $select = $sql->select();

        $this->defaultSql->join(array('category' => $this->table('category_data')),
                                'category.id_category = c.id_parent',
                                array(
                                    'parentName' => 'name',
                                    'parentNameSeo' => 'name_seo'
                                ),
                                $select::JOIN_LEFT);
    }

    public function link ($item) {
//        if ($item['id_parent']) {
//            return $this->url($item['parentNameSeo']) . '/' . $this->url($item['name_seo']);
//        } else {
            return '/' . $this->url($item['name_seo']);
//        }
    }

    final public function listRowByNameSeo ($nameSeo = null)
    {
        $return = $this->listRow(array(
            'c.status' => 1,
            'cd.name_seo' => $nameSeo
        ), true);

        if ($return !== false) {
            $return['link'] = $this->link($return);
        }

        return $return;
    }

    final public function listAll ($where = null, $page = 1, $division = 16, $extendSql = true) {
        $items = parent::listAll ($where, $page, $division, $extendSql);

        if ($items !== false) {
            foreach ($items as $k => $item) {
                $item['link'] = $this->link($item);

                $items[$k] = $item;
            }
        }

        return $items;
    }
}