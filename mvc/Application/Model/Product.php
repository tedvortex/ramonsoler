<?php
namespace Application\Model;

use Zend\Db\Sql\Predicate\Expression;

class Product extends Base
{

    public $table = 'product';

    public $dataColumns = array(
        '*'
    );

    public function getSqlExtend () {
        $select = $this->sql->select();

        $this->defaultSql->join(array('currency' => $this->table('currency')),
                                'currency.id_currency = p.id_currency',
                                array('currency' => 'token')
                               )
                         ->join(array('c' => $this->table('category')),
                               'c.id_category = p.id_category',
                               array())
                         ->join(array('category' => $this->table('category_data')),
                                'category.id_category = p.id_category',
                                array(
                                    'categoryName' => 'name',
                                    'categoryNameSeo' => 'name_seo'
                               ))
                        ->join(array('categoryParent' => $this->table('category')),
                                'categoryParent.id_category = c.id_parent',
                                array(
                                    'categoryParent' => 'id_parent',
                               ),
                               $select::JOIN_LEFT)
                        ->join(array('root_category' => $this->table('category_data')),
                                'root_category.id_category = c.id_parent',
                                array(
                                    'rootCategoryName' => 'name',
                                    'rootCategoryNameSeo' => 'name_seo'
                               ),
                               $select::JOIN_LEFT);
    }

    public function link ($item) {
//        return $this->url($item['rootCategoryNameSeo']) . '/' . $this->url($item['categoryNameSeo']) . '/' . $this->url($item['name_seo']);
        return '/' . $this->url($item['name_seo']);
    }

    final public function listRowByNameSeo ($nameSeo = null)
    {
        $ProductImageModel = \Application\Model\ProductImage::getInstance($this->adapter);

        $return = $this->listRow(array(
            'p.status' => 1,
            'pd.name_seo' => $nameSeo
        ), true);

        if ($return !== false) {
            $return['images'] = $ProductImageModel->listAll(array('id_product' => $return['id_product']));
            $return['link'] = $this->link($return);
        }

        return $return;
    }

    final public function listAll ($where = null, $page = 1, $division = 1000, $extendSql = true) {
        $ProductImageModel = \Application\Model\ProductImage::getInstance($this->adapter);

        $items = parent::listAll ($where, $page, $division, $extendSql);

        if ($items !== false) {
            foreach ($items as $k => $item) {
                $item['images'] = $ProductImageModel->listAll(array('id_product' => $item['id_product']));
                $item['link'] = $this->link($item);

                $items[$k] = $item;
            }
        }

        return $items;
    }
}