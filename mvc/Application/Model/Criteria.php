<?php
namespace Application\Model;

use Zend\Db\Sql\Sql;

class Criteria extends Base
{

    public $table = 'criteria';
    public $dataColumns = array("*");

    public function link ($item) {
        return $this->url($item['name_seo']);
    }

    final public function listRowByNameSeo ($nameSeo = null)
    {
        $return = $this->listRow(array(
            'c.status' => 1,
            'cd.name_seo' => $nameSeo
        ), true);

        if ($return !== false) {
            $return['link'] = $this->link($return);
        }

        return $return;
    }

    final public function listAll ($where = null, $page = 1, $division = 16, $extendSql = true) {
        $items = parent::listAll ($where, $page, $division, $extendSql);

        if ($items !== false) {
            foreach ($items as $k => $item) {
                $item['link'] = $this->link($item);

                $items[$k] = $item;
            }
        }

        return $items;
    }
}