<?php
namespace Application\Model;

use Zend\Db\Sql\Sql;

class Order extends Base
{

    public $table = 'order';

    public function getSql ($extend = false) {
        $sql = new Sql($this->adapter);

        $convertedTable = $this->prefix . $this->prefixSeparator . $this->table;
        $convertedTableKey = mb_substr($this->table, 0, 1);
        $id = $this->identifier . $this->prefixSeparator . $this->table;

        $select = $sql->select();
        $this->defaultSql = $select->from(array(
                mb_substr($this->table, 0, 1) => $convertedTable
            ))
            ->columns($this->columns);

        $this->sql = $sql;

        if ($extend) {
            $this->getSqlExtend();
        }

        return $this->defaultSql;
    }
}