<?php
namespace Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\Annotation\XmlElement;

class Sitemap extends Base
{
    public function sitemapUrl($item, $requestSlash = false)
    {
        $return = array(
            'loc' => 'http://' . $_SERVER['HTTP_HOST'] . ($requestSlash ? '/' : '') . $item['url'],
            'lastmod' => date('Y-m-d'),
            'changefreq' => 'daily',
            'priority' => '0.5',
            'name' => $item['name']
        );

        if (isset($item['images']) && $item['images']) {
            $return['images'] = $item['images'];
        }

        return $return;
    }

    public function defaultAction(Application $app, Request $request)
    {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $ProductModel = \Application\Model\Product::getInstance($app['zend.adapter']);
        $CategoryModel = \Application\Model\Category::getInstance($app['zend.adapter']);

        $routes = $app['engine']['slots']->get('routes');
        $categories = $app['engine']['slots']->get('categories');
        $format = 'xml';
        $sitemap = new \Application\Widget\Sitemap\Urlset();
        $pages = $PageModel->listAll(array(
            'is_sitemap' => 1
        ));

        // list site pages
        if ($pages !== false) {
            foreach ($pages as $p) {
                $p['url'] = $PageModel->link($p);

                $sitemap->addUrl($this->sitemapUrl($p, true));
            }
        }

        // list site categories
        if ($categories !== false) {
            foreach ($categories as $p) {
                $p['url'] = $p['link'];

                $sitemap->addUrl($this->sitemapUrl($p));

                if ($p['subcategories']) {
                    foreach ($p['subcategories'] as $sp) {
                        $sp['url'] = $sp['link'];

                        $sitemap->addUrl($this->sitemapUrl($sp));
                    }
                }
            }
        }

        // list site products
        $products = $ProductModel->listAll();
        if ($products !== false) {
            foreach ($products as $p) {
                $p['url'] = $ProductModel->link($p);
                if ($p['images']) {
                    foreach ($p['images'] as $k => $image) {
                        $p['images'][$k] = $app['engine']['assets']->getUrl('i/imagini-produse/' . $image['image']);
                    }
                }

                $sitemap->addUrl($this->sitemapUrl($p));
            }
        }

        return new Response($app['serializer']->serialize($sitemap, $format), 200, array(
            "Content-Type" => $app['request']->getMimeType($format)
        ));
    }
}