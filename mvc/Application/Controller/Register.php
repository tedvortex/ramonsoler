<?php
namespace Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError;

class Register extends Base {

    public function defaultAction (Application $app, Request $request) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $CustomerModel = \Application\Model\Customer::getInstance($app['zend.adapter']);
        $return = $PageModel->listRow(array('route_name' => 'register'));

        if ($return !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            $form = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                        ->add('first_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Nume',
                                                'minlength' => 3,
                                                'maxlength' => 100
                                            ),
                                            'label' => 'Nume',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('last_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Prenume'
                                            ),
                                            'label' => 'Prenume',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('email', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'email@site.com'
                                            ),
                                            'label' => 'Email',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\Email()
                                            )))
                                        ->add('phone', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '0760 xxx xxx'
                                            ),
                                            'label' => 'Telefon',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('address', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Str. strada Nr. #, etc'
                                            ),
                                            'label' => 'Adresa',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('state', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Alba Iulia'
                                            ),
                                            'label' => 'Judet',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('city', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Alba'
                                            ),
                                            'label' => 'Oras',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('ci', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '1500101102000'
                                            ),
                                            'label' => 'CNP',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('password', 'password', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '******'
                                            ),
                                            'label' => 'Parola',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('send', 'submit', array(
                                            'attr' => array(
                                                'class' => 'btn btn-primary'
                                            ),
                                            'label' => 'Inregistrare membru',
                                            ))
                                        ->getForm();

            $form2 = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                        ->add('company_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Nume firma'
                                            ),
                                            'label' => 'Nume firma',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_cui', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'CUI'
                                            ),
                                            'label' => 'CUI',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_reg', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Nr Reg Com'
                                            ),
                                            'label' => 'Nr Reg Com',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_iban', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'IBAN'
                                            ),
                                            'label' => 'IBAN',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_bank', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Banca'
                                            ),
                                            'label' => 'Banca',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_address', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Adresa'
                                            ),
                                            'label' => 'Adresa',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_phone', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Telefon'
                                            ),
                                            'label' => 'Telefon',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->getForm();

            $form3 = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                        ->add('first_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Nume'
                                            ),
                                            'label' => 'Nume',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('last_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Prenume'
                                            ),
                                            'label' => 'Prenume',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('email', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'email@site.com'
                                            ),
                                            'label' => 'Email',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\Email()
                                            )))
                                        ->add('phone', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '0760 xxx xxx'
                                            ),
                                            'label' => 'Telefon',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('address', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Str. strada Nr. #, etc'
                                            ),
                                            'label' => 'Adresa',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('state', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Alba Iulia'
                                            ),
                                            'label' => 'Judet',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('city', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Alba'
                                            ),
                                            'label' => 'Oras',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('ci', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '1500101102000'
                                            ),
                                            'label' => 'CNP',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('password', 'password', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '******'
                                            ),
                                            'label' => 'Parola',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('send', 'submit', array(
                                            'attr' => array(
                                                'class' => 'btn btn-primary'
                                            ),
                                            'label' => 'Inregistrare firma',
                                        ))
                                        ->getForm();

            if (null !== ($person = $request->get('person'))
                && in_array($person, array('form-fizica', 'form-juridica')))
            {
                $routes = $app['engine']['slots']->get('routes');
                $activateLink = $activateLinkText = $app['engine']['slots']->get('base.path') . $app['url_generator']->generate('activate');

                if ($person == 'form-fizica') {
                    $this->sanitizeForm($form, $request);

                    if ($form->isValid()) {
                        $data = $form->getData();

                        $token = $app['security.encoder.digest']->encodePassword($data['email'], mt_rand(0.1, 99.96));

                        $insert = array(
                            'convertedTable' => array(
                                'status' => 0,
                                'type' => 0,
                                'code' => $token,
                                'first_name' => $data['first_name'],
                                'last_name' => $data['last_name'],
                                'email' => $data['email'],
                                'phone' => $data['phone'],
                                'address' => $data['address'],
                                'state' => $data['state'],
                                'city' => $data['city'],
                                'ci' => $data['ci'],
                                'password' => $app['security.encoder.digest']->encodePassword($data['password'], ''),
                            ),
                        );

                        $result = $CustomerModel->insert($insert);

                        if ($result !== false) {
                            $activateLink .= '?id=' . $result . '&amp;code=' . $this->base64UrlEncode($token);
                            $activateLinkText .= '?id=' . $result . '&code=' . $this->base64UrlEncode($token);

                            $vars = array(
                                'link' => $link = '<a href="' . $activateLink . '">' . $activateLinkText . '</a>',
                                'email' => $data['email'],
                                'password' => $data['password']
                            );

                            $this->templateMail($app, 'register', $data['email'], $vars, 'contact@ramonsoler.ro');

                            return $app['engine']->render('register/success.php');
                        }
                    }
                }

                elseif ($person == 'form-juridica') {
                    $this->sanitizeForm($form2, $request);
                    $this->sanitizeForm($form3, $request);

                    if ($form2->isValid() && $form3->isValid()) {
                        $data = array_merge($form2->getData(), $form3->getData());

                        $token = $app['security.encoder.digest']->encodePassword($data['email'], mt_rand(0.1, 99.96));

                        $insert = array(
                            'convertedTable' => array(
                                'status' => 0,
                                'type' => 1,
                                'code' => $token,
                                'first_name' => $data['first_name'],
                                'last_name' => $data['last_name'],
                                'email' => $data['email'],
                                'phone' => $data['phone'],
                                'address' => $data['address'],
                                'state' => $data['state'],
                                'city' => $data['city'],
                                'ci' => $data['ci'],
                                'password' => $app['security.encoder.digest']->encodePassword($data['password'], ''),
                                'company_name' => $data['company_name'],
                                'company_cui' => $data['company_cui'],
                                'company_reg' => $data['company_reg'],
                                'company_bank' => $data['company_bank'],
                                'company_address' => $data['company_address'],
                                'company_iban' => $data['company_address'],
                                'company_phone' => $data['company_address'],
                            ),
                        );

                        $result = $CustomerModel->insert($insert);

                        if ($result !== false) {
                            $activateLink .= '?id=' . $result . '&amp;code=' . $this->base64UrlEncode($token);
                            $activateLinkText .= '?id=' . $result . '&code=' . $this->base64UrlEncode($token);

                            $vars = array(
                                'link' => $link = '<a href="' . $activateLink . '">' . $activateLinkText . '</a>',
                                'email' => $data['email'],
                                'password' => $data['password']
                            );

                            $this->templateMail($app, 'register', $data['email'], $vars, 'contact@ramonsoler.ro');

                            return $app['engine']->render('register/success.php');
                        }
                    }
                }
            }

            $return['form'] = $form->createView();
            $return['form2'] = $form2->createView();
            $return['form3'] = $form3->createView();

            return $this->httpCacheHandle($app['engine']->render('register/default.php', $return));
        }

        $return = array(
            'code' => 404,
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }
}