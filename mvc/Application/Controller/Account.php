<?php
namespace Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError;

class Account extends Base {
    public function defaultAction (Application $app, Request $request) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $return = $PageModel->findByNameSeo($PageModel->url($request->getRequestUri(), ' ', false));

        if ($return !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            return $this->httpCacheHandle($app['engine']->render('account/default.php', $return));
        }

        $return = array(
            'code' => 404,
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }
    public function profileAction (Application $app, Request $request) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $CustomerModel = \Application\Model\Customer::getInstance($app['zend.adapter']);
        $return = $PageModel->findByNameSeo($PageModel->url($request->getRequestUri(), ' ', false));
        $user = $app['engine']['slots']->get('_user');

        // if user is logged
        if ($return !== false && $user !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            $form = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                        ->add('first_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Nume',
                                                'minlength' => 3,
                                                'maxlength' => 100
                                            ),
                                            'label' => 'Nume',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('last_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Prenume'
                                            ),
                                            'label' => 'Prenume',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('email', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'email@site.com'
                                            ),
                                            'label' => 'Email',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\Email()
                                            )))
                                        ->add('phone', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '0760 xxx xxx'
                                            ),
                                            'label' => 'Telefon',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('address', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Str. strada Nr. #, etc'
                                            ),
                                            'label' => 'Adresa',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('state', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Alba Iulia'
                                            ),
                                            'label' => 'Judet',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('city', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Alba'
                                            ),
                                            'label' => 'Oras',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('ci', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '1500101102000'
                                            ),
                                            'label' => 'CNP',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('password', 'password', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '******'
                                            ),
                                            'label' => 'Parola',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            ),
                                            'required' => false))
                                        ->add('send', 'submit', array(
                                            'attr' => array(
                                                'class' => 'btn btn-primary'
                                            ),
                                            'label' => 'Schimbare date membru',
                                        ))
                                        ->getForm();

            $form2 = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                        ->add('company_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Nume firma'
                                            ),
                                            'label' => 'Nume firma',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_cui', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'CUI'
                                            ),
                                            'label' => 'CUI',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_reg', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Nr Reg Com'
                                            ),
                                            'label' => 'Nr Reg Com',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_iban', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'IBAN'
                                            ),
                                            'label' => 'IBAN',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_bank', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Banca'
                                            ),
                                            'label' => 'Banca',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_address', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Adresa'
                                            ),
                                            'label' => 'Adresa',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_phone', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Telefon'
                                            ),
                                            'label' => 'Telefon',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->getForm();

            $form3 = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                        ->add('first_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Nume'
                                            ),
                                            'label' => 'Nume',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('last_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Prenume'
                                            ),
                                            'label' => 'Prenume',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('email', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'email@site.com'
                                            ),
                                            'label' => 'Email',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\Email()
                                            )))
                                        ->add('phone', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '0760 xxx xxx'
                                            ),
                                            'label' => 'Telefon',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('address', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Str. strada Nr. #, etc'
                                            ),
                                            'label' => 'Adresa',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('state', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Alba Iulia'
                                            ),
                                            'label' => 'Judet',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('city', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Alba'
                                            ),
                                            'label' => 'Oras',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('ci', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '1500101102000'
                                            ),
                                            'label' => 'CNP',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('password', 'password', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '******'
                                            ),
                                            'label' => 'Parola',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            ),
                                            'required' => false))
                                        ->add('send', 'submit', array(
                                            'attr' => array(
                                                'class' => 'btn btn-primary'
                                            ),
                                            'label' => 'Schimbare date firma',
                                        ))
                                        ->getForm();

            // check for user existence
            $customer = $CustomerModel->listRow(array(
                'c.email' => $user->getUsername()
            ));

            // prefill form with data
            if (null !== $customer) {
                $return['type'] = $customer['type'];

                if ($customer['type']) {
//                    $this->sanitizeForm($form3, $request);
//                    $this->sanitizeForm($form2, $request);

                    $form2->setData($customer);
                    $form3->setData($customer);
                } else {
//                    $this->sanitizeForm($form, $request);

                    $form->setData($customer);
                }
            }

            $return['form'] = $form->createView();
            $return['form2'] = $form2->createView();
            $return['form3'] = $form3->createView();

            return $this->httpCacheHandle($app['engine']->render('account/profile.php', $return));
        }

        $return = array(
            'code' => 404,
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }

    public function historyAction (Application $app, Request $request) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $return = $PageModel->findByNameSeo($PageModel->url($request->getRequestUri(), ' ', false));

        if ($return !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            return $this->httpCacheHandle($app['engine']->render('account/history.php', $return));
        }

        $return = array(
            'code' => 404,
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }
}