<?php
namespace Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError;

class Order extends Base {
    public function defaultAction (Application $app, Request $request) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $CustomerModel = \Application\Model\Customer::getInstance($app['zend.adapter']);
        $OrderModel = \Application\Model\Order::getInstance($app['zend.adapter']);
        $OrderProductModel = \Application\Model\OrderProduct::getInstance($app['zend.adapter']);
        $return = $PageModel->findByNameSeo($PageModel->url($request->getRequestUri(), ' ', false));

        if ($return !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            $return['cart'] = $app['session']->get('cart', array());

            $ShopcartModel = new \Application\Model\Shopcart($return['cart']);
            $return['total'] = $ShopcartModel->total();
            $return['count'] = $ShopcartModel->count();

            $form = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                        ->add('first_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Nume',
                                                'minlength' => 3,
                                                'maxlength' => 100
                                            ),
                                            'label' => 'Nume',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('last_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Prenume'
                                            ),
                                            'label' => 'Prenume',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->getForm();

            $form2 = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                        ->add('address', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Str. strada Nr. #, etc'
                                            ),
                                            'label' => 'Adresa',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('state', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Alba Iulia'
                                            ),
                                            'label' => 'Judet',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('city', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Alba'
                                            ),
                                            'label' => 'Oras',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->getForm();

            $form3 = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                        ->add('email', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'email@site.com'
                                            ),
                                            'label' => 'Email',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\Email()
                                            )))
                                        ->add('phone', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '0760 xxx xxx'
                                            ),
                                            'label' => 'Telefon',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('ci', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '1500101102000'
                                            ),
                                            'label' => 'CNP',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
    //                                    ->add('password', 'password', array(
    //                                        'attr' => array(
    //                                            'class' => 'form-control',
    //                                            'placeholder' => '******'
    //                                        ),
    //                                        'label' => 'Parola',
    //                                        'label_attr' => array(
    //                                            'class' => 'col-lg-3 control-label'
    //                                        ),
    //                                        'constraints' => array(
    //                                            new Assert\NotBlank(),
    //                                            new Assert\Length(array('min' => 3, 'max' => 100)),
    //                                        )))
                                        ->getForm();

            $form4 = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                        ->add('observations', 'textarea', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Observatii',
                                                'rows' => 6,
                                            ),
                                            'label' => 'Observatii',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 4, 'max' => 1000)),
                                            ),
                                            'required' => false))
                                        ->add('send', 'submit', array(
                                            'attr' => array(
                                                'class' => 'btn btn-primary'
                                            ),
                                            'label' => 'Comanda rapida',
                                            ))
                                        ->getForm();

            $form_account = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                         ->add('company_name', 'text', array(
                                                'attr' => array(
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Nume firma'
                                                ),
                                                'label' => 'Nume firma',
                                                'label_attr' => array(
                                                    'class' => 'col-lg-3 control-label'
                                                ),
                                                'constraints' => array(
                                                    new Assert\NotBlank(),
                                                    new Assert\Length(array('min' => 3, 'max' => 100)),
                                                )))
                                        ->add('company_cui', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'CUI'
                                            ),
                                            'label' => 'CUI',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_reg', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Nr Reg Com'
                                            ),
                                            'label' => 'Nr Reg Com',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_iban', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'IBAN'
                                            ),
                                            'label' => 'IBAN',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_bank', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Banca'
                                            ),
                                            'label' => 'Banca',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_address', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Adresa'
                                            ),
                                            'label' => 'Adresa',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->add('company_phone', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Telefon'
                                            ),
                                            'label' => 'Telefon',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 3, 'max' => 100)),
                                            )))
                                        ->getForm();

            $return['action'] = $app['security.firewalls']['account']['form']['check_path'];
            $return['error'] = $app['security.last_error']($request);
            $return['last_username'] = $app['session']->get('_security.last_username');

            $user = $app['engine']['slots']->get('_user');

            if (null !== ($step = $request->request->get('step'))
                && in_array($step, array('login', 'order', 'invoice')))
            {
                if ($step == 'login') {
                    // if user is logged
                    if ($user !== false) {
                        // check for user existence
                        $customer = $CustomerModel->listRow(array(
                            'c.email' => $user->getUsername()
                        ));

                        // prefill form with data
                        if (null !== $customer) {
                            if ($customer['type']) {
                                $form_account->setData($customer);
                            }
                            $form->setData($customer);
                            $form2->setData($customer);
                            $form3->setData($customer);
                        }
                    }
                }

                elseif ($step == 'order') {
                    if (null !== ($person = $request->request->get('person'))
                        && in_array($person, array('fizica', 'juridica')))
                    {
                        $routes = $app['engine']['slots']->get('routes');
                        $activateLink = $activateLinkText = $app['engine']['slots']->get('base.path') . $app['url_generator']->generate('activate');

                        if ($person == 'fizica') {
                            // register current user if not logged in
                            $this->sanitizeForm($form, $request);
                            $this->sanitizeForm($form2, $request);
                            $this->sanitizeForm($form3, $request);
                            $this->sanitizeForm($form4, $request);

                            if ($form->isValid() && $form2->isValid() && $form3->isValid()) {
                                $data = array_merge($form->getData(), $form2->getData(), $form3->getData(), $form4->getData());

                                if ($user === false) {
                                    $customer = $CustomerModel->listRow(array(
                                           'c.email' => $data['email']
                                        ));

                                    // if there is no user registered with this email on the website automatically create an account for him
                                    if ($customer === false) {
                                        $data['password'] = substr($app['security.encoder.digest']->encodePassword($data['email'], mt_rand(0.1, 99.96)), 0, 8);

                                        $token = $app['security.encoder.digest']->encodePassword($data['email'], mt_rand(0.1, 99.96));

                                        $insert = array(
                                            'convertedTable' => array(
                                                'status' => 0,
                                                'type' => 0,
                                                'code' => $token,
                                                'first_name' => $data['first_name'],
                                                'last_name' => $data['last_name'],
                                                'email' => $data['email'],
                                                'phone' => $data['phone'],
                                                'address' => $data['address'],
                                                'state' => $data['state'],
                                                'city' => $data['city'],
                                                'ci' => $data['ci'],
                                                'password' => $app['security.encoder.digest']->encodePassword($data['password'], ''),
                                            ),
                                        );

                                        $result = $CustomerModel->insert($insert);

                                        if ($result !== false) {
                                            $data['id_customer'] = $result;

                                            $activateLink .= '?id=' . $result . '&amp;code=' . $this->base64UrlEncode($token);
                                            $activateLinkText .= '?id=' . $result . '&code=' . $this->base64UrlEncode($token);

                                            $vars = array(
                                                'link' => $link = '<a href="' . $activateLink . '">' . $activateLinkText . '</a>',
                                                'email' => $data['email'],
                                                'password' => $data['password']
                                            );

                                            $this->templateMail($app, 'register', $data['email'], $vars, 'contact@ramonsoler.ro');
                                        }
                                    } else {
                                        $data['id_customer'] = $customer['id_customer'];
                                    }
                                } else {
                                    // check if user exists
                                    $customer = $CustomerModel->listRow(array(
                                        'c.email' => $user->getUsername()
                                    ));

                                    if (null !== $customer) {
                                        // set logged in user id
                                        $data['id_customer'] = $customer['id_customer'];
                                    }
                                }

                                $data['type'] = 0;
                                $data['quantity'] = $return['count'];
                                $data['price'] = $return['total'];
                                $data['price_shipping'] = 0;
                                $data['price_total'] = $data['price'] + $data['price_shipping'];
                                $data['observations'] = (string) $data['observations'];
                                unset($data['password']);

                                $return['token'] = $app['security.encoder.digest']->encodePassword(serialize($data), '');

                                $app['session']->set($return['token'], array(
                                        'cart' => $return['cart'],
                                        'data' => $data
                                    ));
                                $app['session']->save();

                                $return['order'] = $data;

                                return $this->httpCacheHandle($app['engine']->render('order/invoice.php', $return));
                            }
                        } else {
                            $this->sanitizeForm($form_account, $request);
                            $this->sanitizeForm($form, $request);
                            $this->sanitizeForm($form2, $request);
                            $this->sanitizeForm($form3, $request);
                            $this->sanitizeForm($form4, $request);

                            if ($form_account->isValid() && $form->isValid() && $form2->isValid() && $form3->isValid() && $form4->isValid()) {
                                $data = array_merge($form_account->getData(), $form->getData(), $form2->getData(), $form3->getData(), $form4->getData());

                                if ($user === false) {
                                    $customer = $CustomerModel->listRow(array(
                                        'c.email' => $data['email']
                                    ));

                                    // register current company if not logged in
                                    // if there is no user registered with this email on the website automatically create an account for him
                                    if ($customer === false) {
                                        $data['password'] = substr($app['security.encoder.digest']->encodePassword($data['email'], mt_rand(0.1, 99.96)), 0, 8);

                                        $token = $app['security.encoder.digest']->encodePassword($data['email'], mt_rand(0.1, 99.96));

                                        $insert = array(
                                            'convertedTable' => array(
                                                'status' => 0,
                                                'type' => 1,
                                                'code' => $token,
                                                'first_name' => $data['first_name'],
                                                'last_name' => $data['last_name'],
                                                'email' => $data['email'],
                                                'phone' => $data['phone'],
                                                'address' => $data['address'],
                                                'state' => $data['state'],
                                                'city' => $data['city'],
                                                'ci' => $data['ci'],
                                                'password' => $app['security.encoder.digest']->encodePassword($data['password'], ''),
                                                'company_name' => $data['company_name'],
                                                'company_cui' => $data['company_cui'],
                                                'company_reg' => $data['company_reg'],
                                                'company_bank' => $data['company_bank'],
                                                'company_address' => $data['company_address'],
                                                'company_iban' => $data['company_address'],
                                                'company_phone' => $data['company_address'],
                                            ),
                                        );

                                        $result = $CustomerModel->insert($insert);

                                        if ($result !== false) {
                                            $data['id_customer'] = $result;

                                            $activateLink .= '?id=' . $result . '&amp;code=' . $this->base64UrlEncode($token);
                                            $activateLinkText .= '?id=' . $result . '&code=' . $this->base64UrlEncode($token);

                                            $vars = array(
                                                'link' => $link = '<a href="' . $activateLink . '">' . $activateLinkText . '</a>',
                                                'email' => $data['email'],
                                                'password' => $data['password']
                                            );

                                            $this->templateMail($app, 'register', $data['email'], $vars, 'contact@ramonsoler.ro');
                                        }
                                    } else {
                                        $data['id_customer'] = $customer['id_customer'];
                                    }
                                } else {
                                    // check if user exists
                                    $customer = $CustomerModel->listRow(array(
                                        'c.email' => $user->getUsername()
                                    ));

                                    if (null !== $customer) {
                                        // set logged in user id
                                        $data['id_customer'] = $customer['id_customer'];
                                    }
                                }

                                $data['type'] = 1;
                                $data['quantity'] = $return['count'];
                                $data['price'] = $return['total'];
                                $data['price_shipping'] = 0;
                                $data['price_total'] = $data['price'] + $data['price_shipping'];
                                $data['observations'] = (string) $data['observations'];
                                unset($data['password']);

                                $return['token'] = $app['security.encoder.digest']->encodePassword(serialize($data), '');

                                $app['session']->set($return['token'], array(
                                    'cart' => $return['cart'],
                                    'data' => $data
                                ));
                                $app['session']->save();

                                $return['order'] = $data;

                                return $this->httpCacheHandle($app['engine']->render('order/invoice.php', $return));
                            }
                        }
                    }
                }

                elseif ($step == "invoice") {
                    // record actual order in our database
                    $token = $request->get('invoice');

                    if ($token !== false) {
                        $invoice = $app['session']->get($token, false);

                        if ($invoice !== false) {
                            // verify $session['cart'] with $invoice['cart'] here
                            $order = $OrderModel->insert(array(
                                'convertedTable' => $invoice['data']
                            ));

                            if ($order !== false) {
                                // insert order products here
                                foreach ($invoice['cart'] as $id => $product) {
                                    $product['id_order'] = $order;
                                    $product['id_product'] = $id;
                                    unset($product['image'], $product['folder'], $product['link']);

                                    $insertedProduct = $OrderProductModel->insert(array(
                                        'convertedTable' => $product
                                    ));
                                }

                                $return['order_no'] = $order;

                                // delete cart in session
                                $app['session']->set('cart', array());
                                $app['session']->save();

                                return $this->httpCacheHandle($app['engine']->render('order/success.php', $return));
                            }
                        }
                    }

                    return $this->httpCacheHandle($app['engine']->render('order/error.php', $return));
                }
            } else {
                // if user is logged
                if ($user !== false) {
                    // check for user existence
                    $customer = $CustomerModel->listRow(array(
                            'c.email' => $user->getUsername()
                        ));

                    // prefill form with data
                    if (null !== $customer) {
                        if ($customer['type']) {
                            $form_account->setData($customer);
                        }
                        $form->setData($customer);
                        $form2->setData($customer);
                        $form3->setData($customer);
                    }
                }
            }

            $return['form'] = $form->createView();
            $return['form2'] = $form2->createView();
            $return['form3'] = $form3->createView();
            $return['form4'] = $form4->createView();
            $return['form_account'] = $form_account->createView();
            $return['form_account2'] = $form->createView();
            $return['form_account3'] = $form2->createView();
            $return['form_account4'] = $form3->createView();
            $return['form_account5'] = $form4->createView();

            if ($return !== false) {
                return $this->httpCacheHandle($app['engine']->render('order/default.php', $return));
            }
        }

        $return = array(
            'code' => 404,
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }
}