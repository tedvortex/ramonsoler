<?php
namespace Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Cart extends Base {

    public function defaultAction (Application $app) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $ProductModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $return = $PageModel->listRow(array('route_name' => 'cart'));

        if ($return !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            $return['cart'] = $app['session']->get('cart', array());
            $ShopcartModel = new \Application\Model\Shopcart($return['cart']);
            $return['total'] = $ShopcartModel->total();
            $return['count'] = $ShopcartModel->count();

            return $app['engine']->render('cart/default.php', $return);
        }

        $return = array(
            'code' => 404,
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }
}