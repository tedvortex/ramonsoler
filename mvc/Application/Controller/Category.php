<?php
namespace Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Category extends Base {
    public function defaultAction (Application $app, Request $request) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $return = $PageModel->findByNameSeo($PageModel->url($request->getRequestUri(), ' ', false));

        if ($return !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            return $this->httpCacheHandle($app['engine']->render('category/default.php', $return));
        }

        $return = array(
            'name' => 'EROARE 404',
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }

    public function listByCategory (Application $app, $categoryNameSeo = null) {
        $CategoryModel = \Application\Model\Category::getInstance($app['zend.adapter']);

        $where = $return = array();

        if ($categoryNameSeo !== null) {
            $return = $CategoryModel->listRowByNameSeo($CategoryModel->url($categoryNameSeo, ' ', false));

            if ($return !== false) {
                $app['engine']['slots']->set('title', $return['title']);
                $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
                $app['engine']['slots']->set('meta_description', $return['meta_description']);

                $where['c.id_parent'] = $return['id_category'];
                $return['subcategories'] = $CategoryModel->listAll($where);
            } else {
                $return = array(
                    'name' => 'EROARE 404',
                    'message' => 'Categoria cautata nu a fost gasita !'
                );

                return $app['engine']->render('404/default.php', $return);
            }
        }

        $app['engine']['slots']->set('categoryCollapsed', $return['id_category']);

        return $app['engine']->render('category/list_categories.php', $return);
    }

    public function listBySubCategory (Application $app, $categoryNameSeo = null, $subCategoryNameSeo = null) {
        $CategoryModel = \Application\Model\Category::getInstance($app['zend.adapter']);
        $ProductModel = \Application\Model\Product::getInstance($app['zend.adapter']);
        $CriteriaModel = \Application\Model\Criteria::getInstance($app['zend.adapter']);
        $OptionModel = \Application\Model\Option::getInstance($app['zend.adapter']);

        $where = $return = array();

        if ($categoryNameSeo !== null) {
            $category = $CategoryModel->listRowByNameSeo($CategoryModel->url($categoryNameSeo, ' ', false));

            if ($category !== false) {
                if ($categoryNameSeo !== null) {
                    $return = $CategoryModel->listRowByNameSeo($CategoryModel->url($subCategoryNameSeo, ' ', false));

                    if ($return !== false) {
                        $app['engine']['slots']->set('title', $return['title']);
                        $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
                        $app['engine']['slots']->set('meta_description', $return['meta_description']);

                        $return['parent'] = $category;

                        $where['p.id_category'] = $return['id_category'];
                    } else {
                        $return = array(
                            'name' => 'EROARE 404',
                            'message' => 'Subcategoria cautata nu a fost gasita !'
                        );

                        return $app['engine']->render('404/default.php', $return);
                    }
                }
            } else {
                $return = array(
                    'name' => 'EROARE 404',
                    'message' => 'Categoria cautata nu a fost gasita !'
                );

                return $app['engine']->render('404/default.php', $return);
            }
        }

        $return['products'] = $ProductModel->listAll($where);
        $return['criteria'] = $CriteriaModel->listAll(array(
            'c.id_category' => $return['id_category']
        ));
        if ($return['criteria'] !== false) {
            foreach ($return['criteria'] as $k => $c) {
                $c['option'] = $OptionModel->listAll(array(
                    'o.id_criteria' => $c['id_criteria']
                ));

                $return['criteria'][$k] = $c;
            }
        }

        $app['engine']['slots']->set('categoryCollapsed', $return['id_category']);

        return $app['engine']->render('category/list_products.php', $return);
    }

    public function listByCategoryProduct (Application $app, $categoryNameSeo = null, $subCategoryNameSeo = null, $productNameSeo = null) {
        $CategoryModel = \Application\Model\Category::getInstance($app['zend.adapter']);
        $ProductModel = \Application\Model\Product::getInstance($app['zend.adapter']);
        $TechnologyModel = \Application\Model\Technology::getInstance($app['zend.adapter']);

        $where = $return = array();

        if ($categoryNameSeo !== null) {
            $rootCategory = $CategoryModel->listRowByNameSeo($CategoryModel->url($categoryNameSeo, ' ', false));

            if ($rootCategory !== false) {
                if ($subCategoryNameSeo !== null) {
                    $category = $CategoryModel->listRowByNameSeo($CategoryModel->url($subCategoryNameSeo, ' ', false));

                    if ($category !== false) {
                        $category['parent'] = $rootCategory;

                        $return = $ProductModel->listRowByNameSeo($ProductModel->url($productNameSeo, ' ', false));

                        if ($return !== false) {
                            $app['engine']['slots']->set('title', $return['title']);
                            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
                            $app['engine']['slots']->set('meta_description', $return['meta_description']);

                            $return['category'] = $category;
                            $app['engine']['slots']->set('categoryCollapsed', $return['category']['id_parent']);
                            $app['engine']['slots']->set('subcategoryCollapsed', $return['category']['id_category']);
                            $return['simmilar'] = $ProductModel->listAll(array(
                                'p.id_category' => $return['category']['id_category'],
                                new \Zend\Db\Sql\Predicate\Expression('p.`id_product` <> ?', $return['id_product'])
                            ), 1, 4);
                            $return['technologies'] = $TechnologyModel->listAllByProduct($return['id_product']);
                        } else {
                            $return = array(
                                'name' => 'EROARE 404',
                                'message' => 'Produsul cautat nu a fost gasit !'
                            );

                            return $app['engine']->render('404/default.php', $return);
                        }
                    } else {
                        $return = array(
                            'name' => 'EROARE 404',
                            'message' => 'Subcategoria cautata nu a fost gasita !'
                        );

                        return $app['engine']->render('404/default.php', $return);
                    }
                } else {
                    $return = array(
                        'name' => 'EROARE 404',
                        'message' => 'Categoria cautata nu a fost gasita !'
                    );

                    return $app['engine']->render('404/default.php', $return);
                }
            } else {
                $return = array(
                    'name' => 'EROARE 404',
                    'message' => 'Categoria cautata nu a fost gasita !'
                );

                return $app['engine']->render('404/default.php', $return);
            }
        }

        return $app['engine']->render('product/default.php', $return);
    }

    public function listByCategoryProductSimple (Application $app, $categoryNameSeo = null, $productNameSeo = null) {
        $CategoryModel = \Application\Model\Category::getInstance($app['zend.adapter']);
        $ProductModel = \Application\Model\Product::getInstance($app['zend.adapter']);
        $TechnologyModel = \Application\Model\Technology::getInstance($app['zend.adapter']);

        $where = $return = array();

        if ($categoryNameSeo !== null) {
            $category = $CategoryModel->listRowByNameSeo($CategoryModel->url($categoryNameSeo, ' ', false));

            if ($category !== false) {
                $category['parent'] = null;

                $return = $ProductModel->listRowByNameSeo($ProductModel->url($productNameSeo, ' ', false));

                if ($return !== false) {
                    $app['engine']['slots']->set('title', $return['title']);
                    $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
                    $app['engine']['slots']->set('meta_description', $return['meta_description']);

                    $return['category'] = $category;
                    $app['engine']['slots']->set('categoryCollapsed', $return['category']['id_parent']);
                    $app['engine']['slots']->set('subcategoryCollapsed', $return['category']['id_category']);
                    $return['simmilar'] = $ProductModel->listAll(array(
                        'p.id_category' => $return['category']['id_category'],
                        new \Zend\Db\Sql\Predicate\Expression('p.`id_product` <> ?', $return['id_product'])
                    ), 1, 4);
                    $return['technologies'] = $TechnologyModel->listAllByProduct($return['id_product']);
                } else {
                    $return = array(
                        'name' => 'EROARE 404',
                        'message' => 'Produsul cautat nu a fost gasit !'
                    );

                    return $app['engine']->render('404/default.php', $return);
                }
            } else {
                $return = array(
                    'name' => 'EROARE 404',
                    'message' => 'Categoria cautata nu a fost gasita !'
                );

                return $app['engine']->render('404/default.php', $return);
            }
        }

        return $app['engine']->render('product/default.php', $return);
    }
}