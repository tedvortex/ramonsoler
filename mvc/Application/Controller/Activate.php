<?php
namespace Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Activate extends Base {
    public function defaultAction (Application $app, Request $request) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $CustomerModel = \Application\Model\Customer::getInstance($app['zend.adapter']);

        $return = $PageModel->findByNameSeo($PageModel->url(preg_replace('%\?.*$%', '', $request->getRequestUri()), ' ', false));

        if ($return !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            $id = $request->get('id');
            $code = $this->base64UrlDecode($request->get('code'));

            $customer = $CustomerModel->listRowById($id);

            if ($customer !== false) {
                if ($customer['code'] == $code) {
                    $CustomerModel->update(array(
                            'convertedTable' => array(
                                'id_customer' => $id
                            )
                        ), array(
                            'convertedTable' => array(
                                'status' => 1,
                                'code' => ''
                            )
                        ));

                    return $this->httpCacheHandle($app['engine']->render('activate/success.php', $return));
                }

                $return['activation_message'] = 'Codul de activare pe care il accesati nu este valid';
            } else {
                $return['activation_message'] = 'Linkul pe care incercati sa il activati nu apartine niciunui utilizator';
            }

            return $this->httpCacheHandle($app['engine']->render('activate/default.php', $return));
        }

        $return = array(
            'code' => 404,
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }
}