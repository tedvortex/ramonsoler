<?php
namespace Application\Controller;

use Symfony\Component\HttpFoundation\Response;
use Silex\Application;

abstract class Base {
    public function base64UrlEncode ($input) {
        return strtr($input, '+/=', '-_,');
    }

    public function base64UrlDecode ($input) {
        return strtr($input, '-_,', '+/=');
    }

    function httpCacheHandle ($body, $code = 200) {
        return new Response($body, $code, array(
                'Cache-Control' => 's-maxage=3600, public'
            ));
    }

    public function email (Application $app, $from = 'Dev', $name = 'Dev', $to, $subject, $message, $reply_email, $attachments = null)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom(array($from => $name))
            ->setReplyTo($reply_email)
            ->setTo((array) $to)
            ->setDate(time())
            ->setBody($message, 'text/html')
            ->addPart(strip_tags(str_replace("<br/>", "\r\n", $message)), 'text/plain');

        if (! empty($attachments)) {
            foreach ($attachments as $a) {
                $message->attach(\Swift_Attachment::fromPath($a));
            }
        }

        $result = $app['mailer']->send($message);

        return ($result ? true : false);
    }

    public function templateMail (Application $app, $template, $emails = array(), $vars, $reply_email) {
        if (! isset($vars['attachment'])) {
            $vars['attachment'] = '';
        }

        $EmailModel = \Application\Model\Email::getInstance($app['zend.adapter']);

        $item = $EmailModel->listRowByTemplate($template);

        if ($item !== false) {
            foreach ($vars as $key => $value) {
                $item['description'] = str_replace("[{$key}]", $value, $item['description']);
                $item['subject'] = str_replace("[{$key}]", $value, $item['subject']);
            }

            if (! is_array($emails)) {
                $emails = explode(',', $emails);
            }

            foreach ($emails as $email) {
                $this->email($app, $item['sender'], $item['sender_name'], $email, $item['subject'],
                    $item['description'], $reply_email, $vars['attachment']);
            }

            return true;
        }

        return false;
    }

    public function sanitizeForm ($form, $request, $data = null) {
        if (null === $data) {
            $data = $request->request->all();
        }
        $children = $form->all();

        // calculate common form values
        $data = array_intersect_key((isset($data['form']) ? $data['form'] : $data), $children);

        // bind correspondent data to this form
        $form->bind($data);

//        // obsolete now
//        $form->handleRequest();
    }
}