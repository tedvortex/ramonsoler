<?php
namespace Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Page extends Base {
    public function defaultAction (Application $app, Request $request) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $return = $PageModel->findByNameSeo($PageModel->url($request->getRequestUri(), ' ', false));

        if ($return !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            return $this->httpCacheHandle($app['engine']->render('page/default.php', $return));
        }

        $return = array(
            'code' => 404,
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }
}