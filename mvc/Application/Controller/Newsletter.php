<?php
namespace Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError;

class Newsletter extends Base {

    public function defaultAction (Application $app) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $return = $PageModel->listRow(array('route_name' => 'newsletter'));

        if ($return !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            $form = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                        ->add('first_name', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Nume'
                                            ),
                                            'label' => 'Nume',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\NotBlank(),
                                                new Assert\Length(array('min' => 4, 'max' => 100)),
                                            )))
                                        ->add('email', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'email@site.com'
                                            ),
                                            'label' => 'Email',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\Email()
                                            )))
                                        ->add('send', 'submit', array(
                                            'attr' => array(
                                                'class' => 'btn btn-primary'
                                            ),
                                            'label' => 'Abonare',
                                            ))
                                        ->getForm();

            $form->handleRequest();

            if ($form->isValid()) {
                $data = $form->getData();
                if ($result !== false) {
                    return $app['engine']->render('newsletter/success.php');
                }
            }

            $return['form'] = $form->createView();

            return $this->httpCacheHandle($app['engine']->render('newsletter/default.php', $return));
        }

        $return = array(
            'code' => 404,
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }
}
