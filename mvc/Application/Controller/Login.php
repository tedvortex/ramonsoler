<?php
namespace Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError;

class Login extends Base {

    public function defaultAction (Application $app, Request $request) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $return = $PageModel->listRow(array('route_name' => 'login'));

        if ($return !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            $return['action'] = $app['security.firewalls']['account']['form']['check_path'];
            $return['error'] = $app['security.last_error']($request);
            $return['last_username'] = $app['session']->get('_security.last_username');

            return $this->httpCacheHandle($app['engine']->render('login/login.php', $return));
        }

        $return = array(
            'code' => 404,
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }

    public function forgotAction (Application $app, Request $request) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $CustomerModel = \Application\Model\Customer::getInstance($app['zend.adapter']);
        $return = $PageModel->listRow(array('route_name' => 'forgot'));

        if ($return !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            $routes = $app['engine']['slots']->get('routes');
            $resetLink = $resetLinkText = $app['engine']['slots']->get('base.path') . $app['url_generator']->generate('reset');

            $form = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
                                        ->add('email', 'text', array(
                                            'attr' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'email@site.com'
                                            ),
                                            'label' => 'Email',
                                            'label_attr' => array(
                                                'class' => 'col-lg-3 control-label'
                                            ),
                                            'constraints' => array(
                                                new Assert\Email()
                                            )))
                                        ->add('send', 'submit', array(
                                            'attr' => array(
                                                'class' => 'btn btn-primary'
                                            ),
                                            'label' => 'Recupereaza parola',
                                        ))
                                        ->getForm();

            $form->handleRequest();

            if ($form->isValid()) {
                $data = $form->getData();

                $customer = $CustomerModel->listRow(array(
                        'c.email' => $data['email']
                    ));

                if ($customer !== false) {
                    $token = $app['security.encoder.digest']->encodePassword($data['email'], mt_rand(0.1, 99.96));

                    $CustomerModel->update(array(
                        'convertedTable' => array(
                            'id_customer' => $customer['id_customer']
                        )
                    ), array(
                        'convertedTable' => array(
                            'code' => $token
                        )
                    ));

                    $resetLink .= '?id=' . $customer['id_customer'] . '&amp;code=' . $this->base64UrlEncode($token);
                    $resetLinkText .= '?id=' . $customer['id_customer'] . '&code=' . $this->base64UrlEncode($token);

                    $vars = array(
                        'link' => $link = '<a href="' . $resetLink . '">' . $resetLinkText . '</a>',
                        'email' => $data['email']
                    );

                    $this->templateMail($app, 'password', $data['email'], $vars, 'contact@ramonsoler.ro');

                    return $app['engine']->render('login/request.php');
                }
            }

            $return['form'] = $form->createView();

            return $this->httpCacheHandle($app['engine']->render('login/forgot.php', $return));
        }

        $return = array(
            'code' => 404,
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }

    public function resetAction (Application $app, Request $request) {
        $PageModel = \Application\Model\Page::getInstance($app['zend.adapter']);
        $CustomerModel = \Application\Model\Customer::getInstance($app['zend.adapter']);
        $return = $PageModel->listRow(array('route_name' => 'reset'));

        if ($return !== false) {
            $app['engine']['slots']->set('title', $return['title']);
            $app['engine']['slots']->set('meta_keywords', $return['meta_keywords']);
            $app['engine']['slots']->set('meta_description', $return['meta_description']);

            $id = $request->get('id');
            $code = $this->base64UrlDecode($request->get('code'));

            $customer = $CustomerModel->listRow(array(
                'c.id_customer' => $id,
                'c.code' => $code
            ));

            if ($customer !== false) {
                $password = substr(crypt($customer['email'], mt_rand(10, 99)), 0, 8);

                if ($customer['code'] == $code) {
                    $CustomerModel->update(array(
                        'convertedTable' => array(
                            'id_customer' => $id
                        )
                    ), array(
                        'convertedTable' => array(
                            'password' => $app['security.encoder.digest']->encodePassword($password, ''),
                            'code' => ''
                        )
                    ));

                    $vars = array(
                        'password' => $password
                    );

                    $this->templateMail($app, 'reset', $customer['email'], $vars, 'contact@ramonsoler.ro');

                    return $this->httpCacheHandle($app['engine']->render('login/password.php', $return));
                }

                $return['reset_message'] = 'Codul de resetare pe care il accesati nu este valid';
            } else {
                $return['reset_message'] = 'Linkul pe care incercati sa il resetati nu apartine niciunui utilizator';
            }

            return $this->httpCacheHandle($app['engine']->render('login/reset.php', $return));
        }

        $return = array(
            'code' => 404,
            'message' => 'Pagina pe care o cautati nu a putut fi gasita !'
        );

        return $app['engine']->render('404/default.php', $return);
    }
}