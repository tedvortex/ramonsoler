<?php
namespace Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Ajax extends Base {

    public function defaultAction (Application $app, Request $request, $module = null, $action = null) {
        $return = array(
            'success' => false
        );

        $id = $request->get('id');
        $quantity = $request->get('quantity');
        $name = $request->get('name');
        $image = $request->get('image');

        if ($module == 'cart') {
            $cart = $app['session']->get('cart', array());
            $ShopcartModel = new \Application\Model\Shopcart($cart);
            $ProductModel = \Application\Model\Product::getInstance($app['zend.adapter']);
            $ProductImageModel = \Application\Model\ProductImage::getInstance($app['zend.adapter']);

            $routes = $app['engine']['slots']->get('routes');

            if ($action == 'add') {
                if ($id !== null
                    && $quantity !== null)
                {
                    $product = $ProductModel->listRow(array(
                        'p.id_product' => $id
                    ), true);

                    if ($product !== false) {
                        $image = $ProductImageModel->listRow(array(
                            'id_product' => $product['id_product']
                        ));

                        $message = $ShopcartModel->add($id, array(
                            'quantity' => $quantity,
                            'name' => $product['name'],
                            'code' => $product['code'],
                            'price' => $product['price'],
                            'image' => $image !== false ? $image['image'] : null,
                            'folder' => '/public/i/imagini-produse/',
                            'link' => $ProductModel->link($product)
                        ));

                        $return['success'] = true;
                        $return['message'] = 'Produsul a fost ' . ($message ? 'adaugat' : 'actualizat') . ' in cos';
                    } else {
                        $return['message'] = 'Produsul nu exista';
                    }
                }
            } elseif ($action == 'update') {
                if ($id !== null
                    && $quantity !== null)
                {
                    $ShopcartModel->update($id, array(
                        'quantity' => $quantity,
                    ));

                    $return['success'] = true;
                    $return['message'] = 'Produsul a fost actualizat in cos';
                }
            } elseif ($action == 'remove') {
                if ($id !== null) {
                    if ($ShopcartModel->remove($id)) {
                        $return['success'] = true;
                        $return['message'] = 'Produsul a fost sters din cos';
                    } else {
                        $return['message'] = 'Produsul nu exista in cos';
                    }
                }
            } elseif ($action == 'list') {
                $return['success'] = true;
                $return['cart'] = $ShopcartModel->storage;
                $return['total'] = $ShopcartModel->total();
                $return['count'] = $ShopcartModel->count();
                $return['text'] = ($return['count'] > 0 ? 'Aveti ' . $return['count'] . ' produs' . ($return['count'] > 1 ? 'e' : '') . ' in cos' : 'Nu aveti niciun produs in cos !');
            }

            $app['session']->set('cart', $ShopcartModel->storage);
            $app['session']->save();
        }

        return $app->json($return, 201);
    }
}