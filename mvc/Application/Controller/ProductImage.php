<?php
namespace Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductImage extends Base {
    public function defaultAction (Application $app, Request $request, $dimension) {
        $file = root . $request->get('folder') . '/' . $request->get('image');
        $fileResized = root . $request->get('folder') . '/' . $dimension . '/' . $request->get('image');

        if (is_file($fileResized)) {
            $image = $app['imagine']->open($fileResized);

            $format = pathinfo($fileResized, PATHINFO_EXTENSION);

            $response = new Response();
            $response->headers->set('Content-type', 'image/'.$format);
            $response->setContent($image->get($format));

            return $response;
        } else {
            if (is_dir(root . $request->get('folder') . '/' . $dimension)) {
                $image = $app['imagine']->open($file);
                $size = $image->getSize();

                if ($image) {
                    $transformation = new \Imagine\Filter\Transformation();
                    $transformation->thumbnail(new \Imagine\Image\Box($dimension, $size->getHeight() * $dimension / $size->getWidth()));
                    $image = $transformation->apply($image);

                    $format = pathinfo($file, PATHINFO_EXTENSION);

                    $response = new Response();
                    $response->headers->set('Content-type', 'image/'.$format);
                    $response->setContent($image->get($format));

                    $image->save($fileResized);

                    return $response;
                }
            } else {
                return false;
            }
        }
    }
}