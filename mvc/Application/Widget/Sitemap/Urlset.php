<?php

namespace Application\Widget\Sitemap;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\XmlNamespace;
use JMS\Serializer\Annotation\XmlList;
use JMS\Serializer\Annotation\XmlElement;

/**
 * @XmlRoot("urlset")
 * @XmlNamespace(uri="http://www.sitemaps.org/schemas/sitemap/0.9")
 * @XmlNamespace(uri="http://www.google.com/schemas/sitemap-image/1.1", prefix="image")
 */
Class Urlset
{

    /**
     * @Type("array<Application\Widget\Sitemap\Url>")
     * @XmlList(inline = true, entry = "url")
     */
    private $urls = array();

    public function addUrl($url)
    {
        $this->urls[] = new Url($url);

    }
}