<?php

namespace Application\Widget\Sitemap;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\XmlNamespace;
use JMS\Serializer\Annotation\XmlList;
use JMS\Serializer\Annotation\XmlElement;

/**
 * @XmlRoot("url")
 */
Class Url
{

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $loc;

    /**
     * @Type("DateTime<'Y-m-d'>")
     * @XmlElement(cdata=false)
     */
    private $lastmod;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $changefreq;

    /**
     * @Type("double")
     * @XmlElement(cdata=false)
     */
    private $priority;

    /**
     * @Type("array<Application\Widget\Sitemap\Image>")
     * @XmlList(inline = true, entry = "image:image")
     * @XmlElement(cdata=false, namespace="http://www.google.com/schemas/sitemap-image/1.1")
     */
    private $images = array();

    public function __construct($url)
    {
        $this->loc = $url['loc'];
        $this->lastmod = new \DateTime($url['lastmod']);
        $this->changefreq = $url['changefreq'];
        $this->priority = $url['priority'];

        if (isset($url['images']) && is_array($url['images'])) {
            foreach ($url['images'] as $k => $image) {
                $this->addImage($image, $url['name']);
            }
        }
    }

    public function addImage($image, $title)
    {
        $this->images[] = new Image($image, $title);
    }
}