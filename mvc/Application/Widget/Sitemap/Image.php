<?php

namespace Application\Widget\Sitemap;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\XmlNamespace;
use JMS\Serializer\Annotation\XmlList;
use JMS\Serializer\Annotation\XmlElement;

/**
 * @XmlRoot("image")
 */
Class Image
{

    /**
     * @Type("string")
     * @XmlElement(cdata=false, namespace="http://www.google.com/schemas/sitemap-image/1.1")
     */
    private $loc;

    /**
     * @Type("string")
     * @XmlElement(cdata=false, namespace="http://www.google.com/schemas/sitemap-image/1.1")
     */
    private $title;

    public function __construct($image, $title)
    {
        $this->loc = $image;
        $this->title = $title;
    }
}